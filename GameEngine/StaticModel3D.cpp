#include "StaticModel3D.h"


StaticModel3D::StaticModel3D(const std::string& name, Material* material, Mesh* mesh, Texture* texture, Transform transform)
: m_Name(name),
m_pMaterial(material),
m_pMesh(mesh),
m_pTexture(texture),
m_Transform(transform)
{
	m_Transform.update();
}


StaticModel3D::~StaticModel3D()
{
}
