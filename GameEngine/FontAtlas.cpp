#include "FontAtlas.h"
#include <Windows.h>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include "helpers.h"

bool LoadFNT(const char* name, CharacterMap& chmap);

FontAtlas::FontAtlas()
{
	m_pTexture = new Texture();
}


FontAtlas::~FontAtlas()
{
	delete m_pTexture;
}

bool FontAtlas::Load(const std::string& fontName)
{
	m_pTexture->load(fontName + ".png");

	if (!LoadFNT((std::string("..\\resourecs\\fonts\\") + fontName + ".fnt").c_str(), m_CharMap))
	{
		return false;
	}

	m_NumChars = m_CharMap.size();
	GenFontForDynamicLabel();
	m_Name = fontName;

	return true;
}

Character& FontAtlas::GetCharacter(wchar_t c)
{
	if (m_CharMap.find(c) != m_CharMap.end())
		return m_CharMap[c];
	else
		return m_CharMap['*'];
}

void FontAtlas::GenFontForDynamicLabel()
{
	int pos_in_buffer = 0;
	std::vector<glm::vec2> positions;
	std::vector<glm::vec2> coords;
	
	for (auto& c : m_CharMap)
	{
		Character& letter = c.second;
		letter.SetPosInBuff(pos_in_buffer++);

		float width = letter.GetWidth();// *m_Scale;
		float height = letter.GetHeight();
		float xoffset = letter.GetXoffset();
		float yoffset = letter.GetYoffset();

		positions.push_back(glm::vec2(xoffset, yoffset));
		positions.push_back(glm::vec2(xoffset + width, yoffset));
		positions.push_back(glm::vec2(xoffset + width, yoffset + height));
		positions.push_back(glm::vec2(xoffset, yoffset + height));

		coords.push_back(glm::vec2((letter.GetX()) / FONT_TEX_SIZE, 1 - (letter.GetY()) / FONT_TEX_SIZE));
		coords.push_back(glm::vec2((letter.GetX() + letter.GetWidth()) / FONT_TEX_SIZE, 1 - (letter.GetY()) / FONT_TEX_SIZE));
		coords.push_back(glm::vec2((letter.GetX() + letter.GetWidth()) / FONT_TEX_SIZE, 1 - (letter.GetY() + letter.GetHeight()) / FONT_TEX_SIZE));
		coords.push_back(glm::vec2((letter.GetX()) / FONT_TEX_SIZE, 1 - (letter.GetY() + letter.GetHeight()) / FONT_TEX_SIZE));
	}

	m_Indicies.resize(6 * (positions.size() / 4));
	for (int i = 0; i < positions.size() / 4; ++i)
	{
		m_Indicies[0 + i * 6] = 0 + i * 4;
		m_Indicies[1 + i * 6] = 2 + i * 4;
		m_Indicies[2 + i * 6] = 1 + i * 4;
		m_Indicies[3 + i * 6] = 0 + i * 4;
		m_Indicies[4 + i * 6] = 3 + i * 4;
		m_Indicies[5 + i * 6] = 2 + i * 4;
	}

	glGenVertexArrays(1, &m_vaoID);
	glBindVertexArray(m_vaoID);

	glGenBuffers(1, &m_vboPositionsID);
	glBindBuffer(GL_ARRAY_BUFFER, m_vboPositionsID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * positions.size(), positions.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), 0);

	glGenBuffers(1, &m_vboCoordsID);
	glBindBuffer(GL_ARRAY_BUFFER, m_vboCoordsID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * coords.size(), coords.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), 0);

	/*glGenBuffers(1, &m_vboIndiciesID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboIndiciesID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort)* indicies.size(), indicies.data(), GL_STATIC_DRAW);*/
}

bool LoadFNT(const char* name, CharacterMap& chmap)
{
	std::ifstream file;
	file.open(name);
	if (!file.good())
	{
		MessageBox(0, L"Font file not gut", 0, 0);
		return false;
	}

	std::string line;
	while (std::getline(file, line))
	{
		//Usuni�cie dubluj�cych si� spacji
		std::string::iterator new_end = std::unique(line.begin(), line.end(), BothAreSpaces);
		line.erase(new_end, line.end());

		//Podzielenie stringu
		std::vector<std::string> strarr = split(line, ' ');

		if (strarr[0] == "char")
		{
			Character c(
				(wchar_t)atoi(split(strarr[1], '=')[1].c_str()),
				(float)atof(split(strarr[2], '=')[1].c_str()),
				(float)atof(split(strarr[3], '=')[1].c_str()),
				(float)atof(split(strarr[4], '=')[1].c_str()),
				(float)atof(split(strarr[5], '=')[1].c_str()),
				(float)atof(split(strarr[6], '=')[1].c_str()),
				(float)atof(split(strarr[7], '=')[1].c_str()),
				(float)atof(split(strarr[8], '=')[1].c_str())
				);
			chmap[c.GetID()] = c;
		}
	}

	file.close();

	return true;
}
