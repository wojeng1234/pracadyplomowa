#ifndef _MY_WINDOW_H
#define _MY_WINDOW_H

#include <string>

class Engine;

class MyWindow
{
private:
	int m_x, m_y;
	int m_width, m_height;
	HWND m_hWnd;
	HINSTANCE m_Hinstance;
	std::wstring m_Name;
	Engine* m_pEngine;

public:
	MyWindow();
	~MyWindow();

	void init(const std::wstring& name, int x, int y, int width, int height, HINSTANCE hInstance, Engine* engine);

	inline int getX() { return m_x; }
	inline int getY() { return m_y; }
	inline int getWidth() { return m_width; }
	inline int getHeight() { return m_height; }
	inline void setWidth(int w) { m_width = w; }
	inline void setHeight(int h) { m_height = h; }
	std::wstring getName() { return m_Name; }
	HWND getHwnd() { return m_hWnd; }
	Engine* getOnwEngine() { return m_pEngine; }

};

#endif
