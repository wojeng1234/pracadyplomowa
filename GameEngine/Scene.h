#ifndef _SCENE_H
#define _SCENE_H

#include <string>

class Scene
{


public:
	Scene();
	~Scene();

	bool Load(const std::string& sceneName);
};

#endif // _SCENE_H