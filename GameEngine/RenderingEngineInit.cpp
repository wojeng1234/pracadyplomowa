#include "RenderingEngine.h"

#include <GL\wglew.h>


RenderingEngine::RenderingEngine()
{
	m_pCamera = NULL;
}

RenderingEngine::~RenderingEngine()
{
	/*for (auto& font : m_Fonts)
		delete font.second;*/

	for (auto& texture : m_Textures)
		delete texture.second;

	for (auto& shader : m_Shaders)
		delete shader.second;

	for (auto& material : m_Materials)
		delete material.second;

	for (auto& mesh : m_Meshes)
		delete mesh.second;

	for (auto& label : m_Labes)
		delete label;

	for (auto& sgmodel : m_StaticGlassModels)
		delete sgmodel.second;

	for (auto& smodel : m_StaticModels)
		delete smodel.second;

	for (auto& light : m_Lights)
		delete light;

	delete m_pCamera;

	glDeleteVertexArrays(1, &m_defaultVAO);

	wglMakeCurrent(0, 0);
	wglDeleteContext(m_hRC);
	ReleaseDC(m_HWND, m_hDC);
}

bool RenderingEngine::Init(HINSTANCE hInstance, MyWindow* window, REND_INIT flag, Engine* engine)
{
	/*if (!InitOldOpenGL(hInstance, hWnd))
	{
	MessageBox(0, L"Old OpenGL Init Failed", 0, 0);
	return false;
	}*/
	m_pWindow = window;
	m_HWND = m_pWindow->getHwnd();
	m_pEngine = engine;

	if (!InitGLEW(hInstance))
	{
		MessageBox(0, L"GLEW Init Failed", 0, 0);
		return false;
	}

	if (!InitOpenGL(hInstance, m_pWindow->getHwnd()))
	{
		MessageBox(0, L"Old OpenGL Init Failed", 0, 0);
		return false;
	}

	glGenVertexArrays(1, &m_defaultVAO);

	//Nawijanie przeciwne do ruchu wskazuwek zegara
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	//Tworzenie shader�w
	this->CreateShader("sprite", SHADER_TYPE::SPRITE);
	this->AddUniformForShader(SHADER_TYPE::SPRITE, "MVP", UNIFORM::MVP);

	this->CreateShader("glass", SHADER_TYPE::GLASS);
	this->AddUniformForShader(SHADER_TYPE::GLASS, "MVP", UNIFORM::MVP);
	this->AddUniformForShader(SHADER_TYPE::GLASS, "color", UNIFORM::COLOR);

	this->CreateShader("font", SHADER_TYPE::FONT);
	this->AddUniformForShader(SHADER_TYPE::FONT, "color", UNIFORM::COLOR);
	this->AddUniformForShader(SHADER_TYPE::FONT, "MVP", UNIFORM::MVP);

	this->CreateShader("line_draw", SHADER_TYPE::LINE_DRAWING);
	this->AddUniformForShader(SHADER_TYPE::LINE_DRAWING, "points", UNIFORM::LINE_POINTS);

	this->CreateShader("directional_light", SHADER_TYPE::DIRECTION_LIGHT);
	this->AddUniformForShader(SHADER_TYPE::DIRECTION_LIGHT, "MVP", UNIFORM::MVP);
	this->AddUniformForShader(SHADER_TYPE::DIRECTION_LIGHT, "M", UNIFORM::M);
	this->AddUniformForShader(SHADER_TYPE::DIRECTION_LIGHT, "N", UNIFORM::N);
	this->AddUniformForShader(SHADER_TYPE::DIRECTION_LIGHT, "matAmbient", UNIFORM::MAT_AMBIENT);
	this->AddUniformForShader(SHADER_TYPE::DIRECTION_LIGHT, "matDiffuse", UNIFORM::MAT_DIFFUSE);
	this->AddUniformForShader(SHADER_TYPE::DIRECTION_LIGHT, "matSpecular", UNIFORM::MAT_SPECULAR);
	this->AddUniformForShader(SHADER_TYPE::DIRECTION_LIGHT, "lightAmbient", UNIFORM::LIGHT_AMBIENT);
	this->AddUniformForShader(SHADER_TYPE::DIRECTION_LIGHT, "lightDiffuse", UNIFORM::LIGHT_DIFFUSE);
	this->AddUniformForShader(SHADER_TYPE::DIRECTION_LIGHT, "lightSpecular", UNIFORM::LIGHT_SPECULAR);
	this->AddUniformForShader(SHADER_TYPE::DIRECTION_LIGHT, "lightDir", UNIFORM::LIGHT_DIR);
	this->AddUniformForShader(SHADER_TYPE::DIRECTION_LIGHT, "eyePosW", UNIFORM::EYE_POS);

	this->CreateShader("point_light", SHADER_TYPE::POINT_LIGHT);
	this->AddUniformForShader(SHADER_TYPE::POINT_LIGHT, "MVP", UNIFORM::MVP);
	this->AddUniformForShader(SHADER_TYPE::POINT_LIGHT, "M", UNIFORM::M);
	this->AddUniformForShader(SHADER_TYPE::POINT_LIGHT, "N", UNIFORM::N);
	this->AddUniformForShader(SHADER_TYPE::POINT_LIGHT, "matAmbient", UNIFORM::MAT_AMBIENT);
	this->AddUniformForShader(SHADER_TYPE::POINT_LIGHT, "matDiffuse", UNIFORM::MAT_DIFFUSE);
	this->AddUniformForShader(SHADER_TYPE::POINT_LIGHT, "matSpecular", UNIFORM::MAT_SPECULAR);
	this->AddUniformForShader(SHADER_TYPE::POINT_LIGHT, "lightAmbient", UNIFORM::LIGHT_AMBIENT);
	this->AddUniformForShader(SHADER_TYPE::POINT_LIGHT, "lightDiffuse", UNIFORM::LIGHT_DIFFUSE);
	this->AddUniformForShader(SHADER_TYPE::POINT_LIGHT, "lightSpecular", UNIFORM::LIGHT_SPECULAR);
	this->AddUniformForShader(SHADER_TYPE::POINT_LIGHT, "lightPosAndRange", UNIFORM::LIGHT_POS_ADN_RANGE);
	this->AddUniformForShader(SHADER_TYPE::POINT_LIGHT, "lightAtt", UNIFORM::LIGHT_ATT);
	this->AddUniformForShader(SHADER_TYPE::POINT_LIGHT, "eyePosW", UNIFORM::EYE_POS);

	this->CreateShader("spot_light", SHADER_TYPE::SPOT_LIGHT);
	this->AddUniformForShader(SHADER_TYPE::SPOT_LIGHT, "MVP", UNIFORM::MVP);
	this->AddUniformForShader(SHADER_TYPE::SPOT_LIGHT, "M", UNIFORM::M);
	this->AddUniformForShader(SHADER_TYPE::SPOT_LIGHT, "N", UNIFORM::N);
	this->AddUniformForShader(SHADER_TYPE::SPOT_LIGHT, "matAmbient", UNIFORM::MAT_AMBIENT);
	this->AddUniformForShader(SHADER_TYPE::SPOT_LIGHT, "matDiffuse", UNIFORM::MAT_DIFFUSE);
	this->AddUniformForShader(SHADER_TYPE::SPOT_LIGHT, "matSpecular", UNIFORM::MAT_SPECULAR);
	this->AddUniformForShader(SHADER_TYPE::SPOT_LIGHT, "lightAmbient", UNIFORM::LIGHT_AMBIENT);
	this->AddUniformForShader(SHADER_TYPE::SPOT_LIGHT, "lightDiffuse", UNIFORM::LIGHT_DIFFUSE);
	this->AddUniformForShader(SHADER_TYPE::SPOT_LIGHT, "lightSpecular", UNIFORM::LIGHT_SPECULAR);
	this->AddUniformForShader(SHADER_TYPE::SPOT_LIGHT, "lightPosAndRange", UNIFORM::LIGHT_POS_ADN_RANGE);
	this->AddUniformForShader(SHADER_TYPE::SPOT_LIGHT, "lightDirAndSpot", UNIFORM::LIGHT_DIR_AND_SPOT);
	this->AddUniformForShader(SHADER_TYPE::SPOT_LIGHT, "lightAtt", UNIFORM::LIGHT_ATT);
	this->AddUniformForShader(SHADER_TYPE::SPOT_LIGHT, "eyePosW", UNIFORM::EYE_POS);

	return true;
}

bool RenderingEngine::InitGLEW(HINSTANCE hInstance)
{
	WNDCLASS wc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hIcon = LoadIcon(0, IDI_APPLICATION);
	wc.hInstance = hInstance;
	wc.lpfnWndProc = DefWindowProc;
	wc.lpszClassName = L"Fake Window";
	wc.lpszMenuName = 0;
	wc.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;

	if (!RegisterClass(&wc))
	{
		MessageBox(0, L"From InitGLEW\nRegister Fake Window Class FAILED!", 0, 0);
		return false;
	}

	HWND hFakeWnd = CreateWindow(
		L"Fake Window",
		L"FAKE",
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		0, 0,
		hInstance,
		0);

	if (!hFakeWnd)
	{
		MessageBox(0, L"From InitGLEW\nCreate Fake Window FAILED!", 0, 0);
		return false;
	}

	HDC FakeDC = GetDC(hFakeWnd);

	PIXELFORMATDESCRIPTOR pfd;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.iLayerType = PFD_MAIN_PLANE;

	int iPixelFormat = ChoosePixelFormat(FakeDC, &pfd);
	if (iPixelFormat == 0)
	{
		MessageBox(0, L"From InitGLEW\niPixelFormat == 0 !", 0, 0);
		return false;
	}

	if (!SetPixelFormat(FakeDC, iPixelFormat, &pfd))
	{
		MessageBox(0, L"From InitGLEW\nSet Pixel Format FAILED!", 0, 0);
		return false;
	}

	HGLRC hFakeRC = wglCreateContext(FakeDC);

	wglMakeCurrent(FakeDC, hFakeRC);

	if (glewInit() != GLEW_OK)
	{
		MessageBox(0, L"From InitGLEW\nGlew Init FAILED!", 0, 0);
		return false;
	}

	wglMakeCurrent(0, 0);
	wglDeleteContext(hFakeRC);
	ReleaseDC(hFakeWnd, FakeDC);
	DestroyWindow(hFakeWnd);
	UnregisterClass(L"Fake Window", hInstance);

	return true;
}

bool RenderingEngine::InitOpenGL(HINSTANCE hInstance, HWND hWnd)
{
	if (WGLEW_ARB_create_context && WGLEW_ARB_pixel_format)
	{
		m_hDC = GetDC(hWnd);

		const int iPixelFormatAttribs[] = {
			WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
			WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
			WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
			WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
			WGL_COLOR_BITS_ARB, 32,
			WGL_DEPTH_BITS_ARB, 24,
			WGL_STENCIL_BITS_ARB, 8,
			WGL_SAMPLES_ARB, 16, // PR�BKI DO MULTISAMPLINGU !!!
			0
		};

		int iContextAttribs[] = {
			WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
			WGL_CONTEXT_MINOR_VERSION_ARB, 4,
			WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
			0
		};

		int iPixelFormat, iNumFormats;
		wglChoosePixelFormatARB(m_hDC, iPixelFormatAttribs, 0, 1, &iPixelFormat, (UINT*)&iNumFormats);

		PIXELFORMATDESCRIPTOR pfd;
		ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
		if (!SetPixelFormat(m_hDC, iPixelFormat, &pfd))
		{
			MessageBox(0, L"From InitOpenGL\nSet Pixel Format FAILED!", 0, 0);
			return false;
		}

		m_hRC = wglCreateContextAttribsARB(m_hDC, 0, iContextAttribs);
		if (!m_hRC)
		{
			MessageBox(0, L"From InitOpenGL\nCreate Context Attribs ARB FAILED!", 0, 0);
			return false;
		}

		wglMakeCurrent(m_hDC, m_hRC);

		return true;
	}
	else
	{
		MessageBox(0, L"From InitOpenGL\nOpenGL Init FAILED!", 0, 0);
		return false;
	}

	return true;
}

bool RenderingEngine::InitOldOpenGL(HINSTANCE hInstance, HWND hWnd)
{
	m_hDC = GetDC(hWnd);
	int PixelFormat;
	PIXELFORMATDESCRIPTOR pfd = {
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_TYPE_RGBA,
		32, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 0, 0,
		PFD_MAIN_PLANE, 0, 0, 0, 0
	};
	PixelFormat = ChoosePixelFormat(m_hDC, &pfd);
	SetPixelFormat(m_hDC, PixelFormat, &pfd);
	m_hRC = wglCreateContext(m_hDC);
	wglMakeCurrent(m_hDC, m_hRC);

	return true;
}