#include "TargetCamera.h"

TargetCamera::TargetCamera()
{
	minRy = -60;
	maxRy = 60;
	minDistance = 1;
	maxDistance = 100;
}
TargetCamera::~TargetCamera() 
{

}

void TargetCamera::update()
{
	glm::mat4 R = glm::yawPitchRoll(yaw, pitch, 0.0f);
	glm::vec3 T = glm::vec3(0, 0, distance);
	T = glm::vec3(R*glm::vec4(T, 0.0f));
	position = target + T;
	look = glm::normalize(target - position);
	up = glm::vec3(R*glm::vec4(0.0f, 1.0f, 0.0f, 0.0f));
	right = glm::cross(look, up);
	viewMatrix = glm::lookAt(position, target, up);
}