#ifndef _ENGINE_H
#define _ENGINE_H

#include "RenderingEngine.h"
#include "Timer.h"
#include "MyWindow.h"
#include "Input.h"
#include "Scene.h"

class Engine
{
	RenderingEngine* m_pRenderingEngine;
	EngineTimer* m_pEngineTimer;
	MyWindow* m_pWindow;
	Input* m_pInput;

	Scene* m_Scene;

	HINSTANCE m_hInstance;

public:
	Engine();
	~Engine();

	void init(
		MyWindow* window,
		EngineTimer* timer,
		RenderingEngine* rendEngine,
		Input* input,
		HINSTANCE hInstance);

	inline RenderingEngine* getRenderingEngine() { return m_pRenderingEngine; }
	inline EngineTimer* getEngineTimer() { return m_pEngineTimer; }
	inline MyWindow* getWindow() { return m_pWindow; }
	inline Input* getInput() { return m_pInput; }

	inline Scene* getScene() { return m_Scene; }

	void ProcessInput();
	void ProcessRender();
};

#endif
