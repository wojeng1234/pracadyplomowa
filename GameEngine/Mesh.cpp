#include "Mesh.h"

bool loadOBJ(
	const char * path,
	std::vector < glm::vec3 > & out_vertices,
	std::vector < glm::vec2 > & out_uvs,
	std::vector < glm::vec3 > & out_normals
	);

Mesh::Mesh()
{
	m_vaoID = -1;
	m_vboPositionsID = -1;
	m_vboCoordsID = -1;
	m_vboNormalsID = -1;
	m_vboIndiciesID = -1;
	m_vertexCount = -1;
	m_indiciesCount = -1;
}

Mesh::~Mesh()
{
	glBindVertexArray(0);
	glDeleteBuffers(1, &m_vboPositionsID);
	glDeleteBuffers(1, &m_vboCoordsID);
	glDeleteBuffers(1, &m_vboNormalsID);
	glDeleteBuffers(1, &m_vboIndiciesID);
	glDeleteVertexArrays(1, &m_vaoID);
}

void Mesh::Draw()
{
	glBindVertexArray(m_vaoID);

	if (m_vertexCount != -1)
		glDrawArrays(GL_TRIANGLES, 0, m_vertexCount);
	else if (m_indiciesCount != -1)
		glDrawElements(GL_TRIANGLES, m_indiciesCount, GL_UNSIGNED_SHORT, 0);
	else;

	glBindVertexArray(0);
}

bool Mesh::LoadLabel(const std::vector<glm::vec2>& positions, const std::vector<glm::vec2>& coords, const std::vector<GLushort>& indicies)
{
	if (m_vaoID != -1)
	{
		MessageBox(0, L"Geometria w tym meshu ju� istnieje.", 0, 0);
		return false;
	}

	m_indiciesCount = indicies.size();

	glGenVertexArrays(1, &m_vaoID);
	glBindVertexArray(m_vaoID);

	glGenBuffers(1, &m_vboPositionsID);
	glBindBuffer(GL_ARRAY_BUFFER, m_vboPositionsID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * positions.size(), positions.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), 0);


	glGenBuffers(1, &m_vboCoordsID);
	glBindBuffer(GL_ARRAY_BUFFER, m_vboCoordsID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * coords.size(), coords.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), 0);

	glGenBuffers(1, &m_vboIndiciesID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboIndiciesID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort)* indicies.size(), indicies.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);

	return true;
}

bool Mesh::LoadFromObj(const std::string& meshName)
{
	if (m_vaoID != -1)
	{
		MessageBox(0, L"Geometria w tym meshu ju� istnieje.", 0, 0);
		return false;
	}

	std::vector<glm::vec3> pos;
	std::vector<glm::vec2> coords;
	std::vector<glm::vec3> normals;
	if (!loadOBJ((std::string("..\\resourecs\\meshes\\") + meshName + ".obj").c_str(), pos, coords, normals))
	{
		MessageBox(0, L"B��d wczytywania modelu OBJ.", 0, 0);
		return false;
	}

	m_vertexCount = pos.size();

	glGenVertexArrays(1, &m_vaoID);
	glGenBuffers(1, &m_vboPositionsID);
	glGenBuffers(1, &m_vboCoordsID);
	glGenBuffers(1, &m_vboNormalsID);

	glBindVertexArray(m_vaoID);
	glBindBuffer(GL_ARRAY_BUFFER, m_vboPositionsID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * m_vertexCount, pos.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_vboCoordsID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * m_vertexCount, coords.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_vboNormalsID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * m_vertexCount, normals.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), 0);

	glBindVertexArray(0);

	return true;
}

bool loadOBJ(
	const char * path,
	std::vector < glm::vec3 > & out_vertices,
	std::vector < glm::vec2 > & out_uvs,
	std::vector < glm::vec3 > & out_normals
	)
{
	std::vector< unsigned int > vertexIndices, uvIndices, normalIndices;
	std::vector< glm::vec3 > temp_vertices;
	std::vector< glm::vec2 > temp_uvs;
	std::vector< glm::vec3 > temp_normals;

	FILE * file = fopen(path, "r");
	if (file == NULL)
	{
		printf("Impossible to open the file !\n");
		return false;
	}

	while (1)
	{

		char lineHeader[128];
		// read the first word of the line
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		// else : parse lineHeader
		if (strcmp(lineHeader, "v") == 0)
		{
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0)
		{
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0)
		{
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0)
		{
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9)
			{
				printf("File can't be read by our simple parser : ( Try exporting with other options\n");
				return false;
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
	}
	// For each vertex of each triangle
	for (unsigned int i = 0; i < vertexIndices.size(); i++)
	{
		glm::vec3 vertex = temp_vertices[vertexIndices[i] - 1];
		out_vertices.push_back(vertex);
	}
	for (unsigned int i = 0; i < uvIndices.size(); i++)
	{
		glm::vec2 uv = temp_uvs[uvIndices[i] - 1];
		out_uvs.push_back(uv);
	}
	for (unsigned int i = 0; i < normalIndices.size(); i++)
	{
		glm::vec3 normal = temp_normals[normalIndices[i] - 1];
		out_normals.push_back(normal);
	}
	return true;
}