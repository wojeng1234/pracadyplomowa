#ifndef _SHADER_H
#define _SHADER_H

#include <GL/glew.h>
#include <string>
#include <map>


enum UNIFORM
{
	MVP,
	MV,
	M,
	N,
	
	MAT_AMBIENT,
	MAT_DIFFUSE,
	MAT_SPECULAR, //specular + shininess

	LIGHT_AMBIENT,
	LIGHT_DIFFUSE,
	LIGHT_SPECULAR, //specular + shininess

	LIGHT_POS_ADN_RANGE,
	LIGHT_DIR,
	LIGHT_DIR_AND_SPOT,
	LIGHT_ATT,

	EYE_POS,
	COLOR,
	TRANSLATION,
	SCR_WIDTH_HEIGHT,
	LINE_POINTS,
};

class Shader
{
	GLuint m_Program;
	std::map<UNIFORM, GLuint> m_Uniforms;

public:
	Shader();
	~Shader();

	void createProgram(const std::string &name);
	void use();
	void unUse();
	void addUniform(const std::string &uniformName, UNIFORM uniform);
	inline GLuint getUniform(const UNIFORM &uniform) { return m_Uniforms[uniform]; }
};

#endif
