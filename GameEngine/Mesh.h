#ifndef _MESH_H
#define _MESH_H

#include <iostream>
#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>

class Mesh
{
	GLuint m_vaoID;
	GLuint m_vboPositionsID;
	GLuint m_vboCoordsID;
	GLuint m_vboNormalsID;
	GLuint m_vboIndiciesID;
	int m_vertexCount;
	int m_indiciesCount;

public:
	Mesh();
	~Mesh();

	void Draw();

	inline void Bind() { glBindVertexArray(m_vaoID); }
	inline void unBind() { glBindVertexArray(0); }

	bool LoadFromObj(const std::string& meshName);
	bool LoadLabel(const std::vector<glm::vec2>& positions, const std::vector<glm::vec2>& coords, const std::vector<GLushort>& indicies);
	inline int GetVertexCount() { return m_vertexCount; }
};

#endif // _MESH_H
