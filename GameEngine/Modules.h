#ifndef _MODULES_H
#define _MODULES_H

//Temporary
#ifndef SCR_WIDTH
#define SCR_WIDTH 1280
#endif
#ifndef SCR_HEIGHT
#define SCR_HEIGHT 960
#endif

#include <Windows.h>
#define WIN32_LEAN_AND_MEAN

#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <glm\glm.hpp>
#include <GL\glew.h>

#include "Bitmap.h"
#include "Character.h"
#include "DynamicLabel.h"
#include "FontAtlas.h"
#include "GlobalWndProc.h"
#include "Material.h"
#include "Mesh.h"
#include "MyWindow.h"
#include "RenderingEngine.h"
#include "Shader.h"
#include "StaticLabel.h"
#include "Texture.h"
#include "WindowsManager.h"

#endif