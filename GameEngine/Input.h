#ifndef _INPUT_H
#define _INPUT_H

#include "VirtualKeys.h"

namespace buttons
{
	enum BUTTON
	{
		UP = XINPUT_GAMEPAD_DPAD_UP,
		DOWN = XINPUT_GAMEPAD_DPAD_DOWN,
		LEFT = XINPUT_GAMEPAD_DPAD_LEFT,
		RIGHT = XINPUT_GAMEPAD_DPAD_RIGHT,
		START = XINPUT_GAMEPAD_START,
		BACK = XINPUT_GAMEPAD_BACK,
		LEFT_THUMB = XINPUT_GAMEPAD_LEFT_THUMB,
		RIGHT_THUMB = XINPUT_GAMEPAD_RIGHT_THUMB,
		LEFT_SHOULDER = XINPUT_GAMEPAD_LEFT_SHOULDER,
		RIGHT_SHOULDER = XINPUT_GAMEPAD_RIGHT_SHOULDER,
		A = XINPUT_GAMEPAD_A,
		B = XINPUT_GAMEPAD_B,
		X = XINPUT_GAMEPAD_X,
		Y = XINPUT_GAMEPAD_Y
	};
}

class Input
{
private:
	XINPUT_STATE gamePadState;

	int SCREEN_WIDTH_HALF;
	int SCREEN_HEIGHT_HALF;

	int xDelta;
	int yDelta;
	int xScrPos;
	int yScrPos;
	int xWndPos;
	int yWndPos;

	bool keys[256];
	bool keysUp[256];
	bool keysDown[256];

	bool LeftButton;
	bool RightButton;
	bool LeftButtonUp;
	bool RightButtonUp;
	bool LeftButtonDown;
	bool RightButtonDown;

	bool isCursorLocked;

public:
	Input();
	~Input();

	void update();
	//SETTERS FOR MOUSE POSITION
	inline void setXWndPos(int x)				{ xWndPos = x; }
	inline void setYWndPos(int y)				{ yWndPos = y; }
	inline void setXYPos(int x, int y)			{ xScrPos = x; yScrPos = y; }
	inline void setXPos(int x)					{ xScrPos = x; }
	inline void setYPos(int y)					{ yScrPos = y; }
	inline void setXYDelta(int dx, int dy)		{ xDelta = dx; yDelta = dy; }
	inline void setXDelta(int dx)				{ xDelta = dx; }
	inline void setYDelta(int dy)				{ yDelta = dy; }

	//SETTERS FOR BUTTONS
	inline void setKeyDown(int key)				{ keys[key] = true; keysDown[key] = true; }
	inline void setKeyUp(int key)				{ keys[key] = false; keysUp[key] = true; }
	inline void setLeftButtonDonw()				{ LeftButton = true; LeftButtonDown = true; }
	inline void setLeftButtonUp()				{ LeftButton = false; LeftButtonUp = true; }
	inline void setRightButtonDonw()			{ RightButton = true; RightButtonDown = true; }
	inline void setRightButtonUp()				{ RightButton = false; RightButtonUp = true; }

	//SETTERS FOR CURSOR STATE
	inline void setCursorPos(int x, int y)		{ SetCursorPos(x, y); }
	inline void hideCursor()					{ ShowCursor(false); }
	inline void showCursor()					{ ShowCursor(true); }
	inline void getCursorPos(int &x, int &y)	{ POINT pos; GetCursorPos(&pos); x = pos.x; y = pos.y; }
	inline void lockCursor()					{ isCursorLocked = true; }
	inline void unlockCursor()					{ isCursorLocked = false; }
	inline bool isMouseLocked()					{ return isCursorLocked; }

	//GETTERS FOR MOUSE POSITION
	inline const int  getXWndPos()		{ return xWndPos; }
	inline const int  getYWndPos()		{ return yWndPos; }
	inline const int  getXScrPos()		{ return xScrPos; }
	inline const int  getYScrPos()		{ return yScrPos; }
	inline const int  getXDelta()		{ return xDelta; }
	inline const int  getYDelta()		{ return yDelta; }

	//GETTERS FOR MOUSE BUTTONS
	inline const bool getKeyState(int key)				{ return keys[key]; }
	inline const bool getLeftButtonState(int key)		{ return LeftButton; }
	inline const bool getRightButtonState(int key)		{ return RightButton; }
	inline const bool isLeftButtonDown()				{ return LeftButtonDown; }
	inline const bool isRightButtonDown()				{ return RightButtonDown; }
	inline const bool isLeftButtonUp()					{ return LeftButtonUp; }
	inline const bool isRightButtonUp()					{ return RightButtonUp; }
	inline const bool isKeyDown(int key)				{ return keysDown[key]; }
	inline const bool isKeyUp(int key)					{ return keysUp[key]; }

	//XBOX CONTROLLER GETTERS
	inline const SHORT getLeftThumbX()					{ return gamePadState.Gamepad.sThumbLX; }
	inline const SHORT getLeftThumbY()					{ return gamePadState.Gamepad.sThumbLY; }
	inline const SHORT getRightThumbX()					{ return gamePadState.Gamepad.sThumbRX; }
	inline const SHORT getRightThumbY()					{ return gamePadState.Gamepad.sThumbRY; }
	inline const BYTE  getLeftTrigger()					{ return gamePadState.Gamepad.bLeftTrigger; }
	inline const BYTE  getRightTrigger()				{ return gamePadState.Gamepad.bRightTrigger; }
	inline const int  isPadButtonDown(buttons::BUTTON button)	{ return (button & gamePadState.Gamepad.wButtons); }


};

#endif // _INPUT_H
