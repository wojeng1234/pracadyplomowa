#include "RenderingEngine.h"

#include <GL\wglew.h>
#include <glm\gtc\matrix_inverse.hpp>
#include <glm\gtc\type_ptr.hpp>
#include "Engine.h"

void RenderingEngine::DrawStaticStaff()
{
	m_pCamera->update();

	DrawStaticModles3D();
	DrawStaticGlass3D();
	DrawStaticLabels();
}

void RenderingEngine::DrawLine(const glm::vec2& p1, const glm::vec2& p2)
{
	glBindVertexArray(m_defaultVAO);
	Shader* shader = m_Shaders[SHADER_TYPE::LINE_DRAWING];
	shader->use();
	glUniform4f(shader->getUniform(UNIFORM::LINE_POINTS), p1.x, p1.y, p2.x, p2.y);
	glDrawArrays(GL_LINES, 0, 2);
	shader->unUse();
}

void RenderingEngine::DrawStaticLabels()
{
	int w = m_pEngine->getWindow()->getWidth();
	int h = m_pEngine->getWindow()->getHeight();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);
	for (auto& label : m_Labes)
	{
		label->getTransform().setScale(glm::vec3(label->getScale() / w, -label->getScale() / h, 1.0f));
		label->getTransform().update();
		Texture* texture = label->getTexture();
		Shader* shader = label->getShader();
		Mesh* mesh = label->getMesh();
		texture->bind();
		shader->use();
		glUniform3f(shader->getUniform(UNIFORM::COLOR), label->getColor().r, label->getColor().g, label->getColor().b);
		glUniformMatrix4fv(shader->getUniform(UNIFORM::MVP), 1, GL_FALSE, &(label->getTransform().getModelMatrix())[0][0]);
		mesh->Draw();
		shader->unUse();
		texture->unBind();
	}
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
}

void RenderingEngine::DrawStaticModles3D()
{
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	
	Shader* shader = m_Shaders[SHADER_TYPE::SPRITE];
	shader->use();
	for (auto& model : m_StaticModels)
	{
		StaticModel3D* currModel = model.second;
		currModel->getTexture()->bind();
		currModel->getTransform().update();

		glm::mat4 MV =
			m_pCamera->getViewMatrix() *
			currModel->getTransform().getModelMatrix();
		glm::mat4 MVP =
			m_pCamera->getProjectionMatrix() * MV;

		glUniformMatrix4fv(shader->getUniform(UNIFORM::MVP), 1, GL_FALSE, glm::value_ptr(MVP));

		currModel->getMesh()->Draw();
		currModel->getTexture()->unBind();
	}
	shader->unUse();
	

	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	glDepthMask(GL_FALSE);
	glDepthFunc(GL_EQUAL);

	for (int i = 0; i < m_DirectionalLights.size(); ++i)
	{
		Shader* shader = m_Shaders[SHADER_TYPE::DIRECTION_LIGHT];
		shader->use();
		glUniform4fv(shader->getUniform(UNIFORM::LIGHT_AMBIENT), 1, glm::value_ptr(m_DirectionalLights[i]->getAmbient()));
		glUniform4fv(shader->getUniform(UNIFORM::LIGHT_DIFFUSE), 1, glm::value_ptr(m_DirectionalLights[i]->getDiffuse()));
		glUniform4fv(shader->getUniform(UNIFORM::LIGHT_SPECULAR), 1, glm::value_ptr(m_DirectionalLights[i]->getSpecular()));
		glUniform3fv(shader->getUniform(UNIFORM::LIGHT_DIR), 1, glm::value_ptr(m_DirectionalLights[i]->getDirection()));
		glUniform3fv(shader->getUniform(UNIFORM::EYE_POS), 1, glm::value_ptr(m_pCamera->getPos()));
		//glUniform3fv(shader->getUniform(UNIFORM::EYE_POS), 1, glm::value_ptr(glm::vec3(0, 0, 0)));
		for (auto& model : m_StaticModels)
		{
			StaticModel3D* currModel = model.second;
			currModel->getTexture()->bind();
			currModel->getTransform().update();

			glm::mat4 MV =
				m_pCamera->getViewMatrix() *
				currModel->getTransform().getModelMatrix();
			glm::mat4 MVP =
				m_pCamera->getProjectionMatrix() * MV;
			glm::mat3 N = glm::inverseTranspose(glm::mat3(MV));

			glUniformMatrix4fv(shader->getUniform(UNIFORM::M), 1, GL_FALSE, glm::value_ptr(currModel->getTransform().getModelMatrix()));
			glUniformMatrix4fv(shader->getUniform(UNIFORM::MVP), 1, GL_FALSE, glm::value_ptr(MVP));
			glUniformMatrix3fv(shader->getUniform(UNIFORM::N), 1, GL_FALSE, glm::value_ptr(glm::inverseTranspose(glm::mat3(currModel->getTransform().getModelMatrix()))));
			//glUniformMatrix3fv(shader->getUniform(UNIFORM::N), 1, GL_FALSE, glm::value_ptr(N));
			glUniform4fv(shader->getUniform(UNIFORM::MAT_AMBIENT), 1, glm::value_ptr(currModel->getMaterial()->GetAmbient()));
			glUniform4fv(shader->getUniform(UNIFORM::MAT_DIFFUSE), 1, glm::value_ptr(currModel->getMaterial()->GetDiffuse()));
			glUniform4fv(shader->getUniform(UNIFORM::MAT_SPECULAR), 1, glm::value_ptr(currModel->getMaterial()->GetSpecular()));

			currModel->getMesh()->Draw();
			currModel->getTexture()->unBind();
		}

		shader->unUse();
	}

	for (int i = 0; i < m_PointLights.size(); ++i)
	{
		Shader* shader = m_Shaders[SHADER_TYPE::POINT_LIGHT];
		shader->use();
		glUniform4fv(shader->getUniform(UNIFORM::LIGHT_AMBIENT), 1, &m_PointLights[i]->getAmbient()[0]);
		glUniform4fv(shader->getUniform(UNIFORM::LIGHT_DIFFUSE), 1, &m_PointLights[i]->getDiffuse()[0]);
		glUniform4fv(shader->getUniform(UNIFORM::LIGHT_SPECULAR), 1, &m_PointLights[i]->getSpecular()[0]);
		glm::vec4 posAndRange(m_PointLights[i]->getPos(), m_PointLights[i]->getRange());
		glUniform4fv(shader->getUniform(UNIFORM::LIGHT_POS_ADN_RANGE), 1, &posAndRange[0]);
		glUniform3fv(shader->getUniform(UNIFORM::LIGHT_ATT), 1, &m_PointLights[i]->getAtt()[0]);
		glUniform3fv(shader->getUniform(UNIFORM::EYE_POS), 1, &m_pCamera->getPos()[0]);
		
		for (auto& model : m_StaticModels)
		{
			StaticModel3D* currModel = model.second;
			currModel->getTexture()->bind();
			currModel->getTransform().update();

			glm::mat4 MV =
				m_pCamera->getViewMatrix() *
				currModel->getTransform().getModelMatrix();
			glm::mat4 MVP =
				m_pCamera->getProjectionMatrix() * MV;
			glm::mat3 N = glm::inverseTranspose(glm::mat3(MV));

			glUniformMatrix4fv(shader->getUniform(UNIFORM::M), 1, GL_FALSE, &currModel->getTransform().getModelMatrix()[0][0]);
			glUniformMatrix4fv(shader->getUniform(UNIFORM::MVP), 1, GL_FALSE, &MVP[0][0]);
			glUniformMatrix3fv(shader->getUniform(UNIFORM::N), 1, GL_FALSE, &N[0][0]);
			glUniform4fv(shader->getUniform(UNIFORM::MAT_AMBIENT), 1, &currModel->getMaterial()->GetAmbient()[0]);
			glUniform4fv(shader->getUniform(UNIFORM::MAT_DIFFUSE), 1, &currModel->getMaterial()->GetDiffuse()[0]);
			glUniform4fv(shader->getUniform(UNIFORM::MAT_SPECULAR), 1, &currModel->getMaterial()->GetSpecular()[0]);

			currModel->getMesh()->Draw();
			
			currModel->getTexture()->unBind();
		}

		shader->unUse();
	}
	
	for (int i = 0; i < m_SpotLights.size(); ++i)
	{
		Shader* shader = m_Shaders[SHADER_TYPE::SPOT_LIGHT];
		shader->use();
		glUniform4fv(shader->getUniform(UNIFORM::LIGHT_AMBIENT), 1, &m_SpotLights[i]->getAmbient()[0]);
		glUniform4fv(shader->getUniform(UNIFORM::LIGHT_DIFFUSE), 1, &m_SpotLights[i]->getDiffuse()[0]);
		glUniform4fv(shader->getUniform(UNIFORM::LIGHT_SPECULAR), 1, &m_SpotLights[i]->getSpecular()[0]);
		glm::vec4 posAndRange(m_SpotLights[i]->getPos(), m_SpotLights[i]->getRange());
		glUniform4fv(shader->getUniform(UNIFORM::LIGHT_POS_ADN_RANGE), 1, &posAndRange[0]);
		glm::vec4 dirAndSpot(m_SpotLights[i]->getDir(), m_SpotLights[i]->getSpot());
		glUniform4fv(shader->getUniform(UNIFORM::LIGHT_DIR_AND_SPOT), 1, &dirAndSpot[0]);
		glUniform3fv(shader->getUniform(UNIFORM::LIGHT_ATT), 1, &m_SpotLights[i]->getAtt()[0]);
		glUniform3fv(shader->getUniform(UNIFORM::EYE_POS), 1, &m_pCamera->getPos()[0]);

		for (auto& model : m_StaticModels)
		{
			StaticModel3D* currModel = model.second;
			currModel->getTexture()->bind();
			currModel->getTransform().update();

			glm::mat4 MV =
				m_pCamera->getViewMatrix() *
				currModel->getTransform().getModelMatrix();
			glm::mat4 MVP =
				m_pCamera->getProjectionMatrix() * MV;
			glm::mat3 N = glm::inverseTranspose(glm::mat3(MV));

			glUniformMatrix4fv(shader->getUniform(UNIFORM::M), 1, GL_FALSE, &currModel->getTransform().getModelMatrix()[0][0]);
			glUniformMatrix4fv(shader->getUniform(UNIFORM::MVP), 1, GL_FALSE, &MVP[0][0]);
			glUniformMatrix3fv(shader->getUniform(UNIFORM::N), 1, GL_FALSE, &N[0][0]);
			glUniform4fv(shader->getUniform(UNIFORM::MAT_AMBIENT), 1, &currModel->getMaterial()->GetAmbient()[0]);
			glUniform4fv(shader->getUniform(UNIFORM::MAT_DIFFUSE), 1, &currModel->getMaterial()->GetDiffuse()[0]);
			glUniform4fv(shader->getUniform(UNIFORM::MAT_SPECULAR), 1, &currModel->getMaterial()->GetSpecular()[0]);

			currModel->getMesh()->Draw();

			currModel->getTexture()->unBind();
		}

		shader->unUse();
	}
	
	glDepthFunc(GL_LESS);
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	/*Shader* shader = m_Shaders[SHADER_TYPE::SPRITE];
	for (auto& model : m_StaticModels)
	{
		glm::mat4 MVP =
			m_pCamera->getProjectionMatrix() *
			m_pCamera->getViewMatrix() *
			model.second->getTransform().getModelMatrix();
		Texture* texture = model.second->getTexture();
		texture->bind();
		shader->use();
		model.second->getTransform().update();
		glUniformMatrix4fv(shader->getUniform(UNIFORM::MVP), 1, GL_FALSE, &MVP[0][0]);
		model.second->getMesh()->Draw();
		shader->unUse();
		texture->unBind();
	}*/
}

void RenderingEngine::DrawStaticGlass3D()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	Shader* shader = m_Shaders[SHADER_TYPE::GLASS];
	for (auto& glass : m_StaticGlassModels)
	{
		glm::mat4 MVP =
			m_pCamera->getProjectionMatrix() *
			m_pCamera->getViewMatrix() *
			glass.second->getTransform().getModelMatrix();
		shader->use();
		glUniformMatrix4fv(shader->getUniform(UNIFORM::MVP), 1, GL_FALSE, &MVP[0][0]);
		glUniform4f(shader->getUniform(UNIFORM::COLOR), 
			glass.second->getColor().r,
			glass.second->getColor().g,
			glass.second->getColor().b,
			glass.second->getColor().a);
		glass.second->getMesh()->Draw();
		shader->unUse();
	}

	glDisable(GL_BLEND);
}

void RenderingEngine::DrawDynamicLabel(
	const std::string& fontName,
	const std::wstring& label,
	const glm::vec2& translation,
	const glm::vec3& color,
	float scale)
{
	int w = m_pEngine->getWindow()->getWidth();
	int h = m_pEngine->getWindow()->getHeight();
	FontAtlas* font = m_Fonts[fontName];
	Texture* texture = font->getTexture();
	Shader* shader = m_Shaders[SHADER_TYPE::FONT];
	Transform tr;
	tr.setPosition(glm::vec3(translation, 0.0f));
	tr.setScale(glm::vec3(scale / w, -scale / h, 1.0f));
	tr.update();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);
	glBindVertexArray(font->GetVAO());
	texture->bind();
	shader->use();
	for (auto& c : label)
	{
		Character letter = font->GetCharacter(c);
		glUniform3f(shader->getUniform(UNIFORM::COLOR), color.r, color.g, color.b);
		glUniformMatrix4fv(shader->getUniform(UNIFORM::MVP), 1, GL_FALSE, &(tr.getModelMatrix())[0][0]);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, (font->getIndicies() + letter.GetPosInBuff() * 6));
		tr.getPosition().x += letter.GetXadvance() / (w / scale);
		tr.update();
	}
	shader->unUse();
	texture->unBind();
	glBindVertexArray(0);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
}

void RenderingEngine::ClearBuffers(GLbitfield bufflag)
{
	glClear(bufflag);
}

void RenderingEngine::SetClearColor(const glm::vec4& color)
{
	glClearColor(color.r, color.g, color.b, color.a);
}

void RenderingEngine::SwapBuffers()
{
	::SwapBuffers(m_hDC);
}