#ifndef _TRANSFORM_H
#define _TRANSFORM_H

#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>

class Transform
{
private:
	glm::mat4 m_ModelMatrix;
	glm::vec3 m_Position;
	glm::vec3 m_Rotation;
	glm::vec3 m_Scale;

public:
	Transform() 
	{
		m_Position = glm::vec3(0, 0, 0);
		m_Rotation = glm::vec3(0, 0, 0);
		m_Scale = glm::vec3(1, 1, 1);
	}
	Transform(const glm::vec3& pos, const glm::vec3& rot, const glm::vec3& scale):
		m_Position(pos), m_Rotation(rot), m_Scale(scale)
	{}

	~Transform() {}

	inline void setPosition(const glm::vec3 &pos)		{ m_Position = pos; }
	inline void setRotation(const glm::vec3 &rot)		{ m_Rotation = rot; }
	inline void setScale(const glm::vec3 &sca)			{ m_Scale = sca; }
	inline void setModelMatrix(const glm::mat4 &m)		{ m_ModelMatrix = m; }

	inline glm::vec3& getPosition() { return m_Position; }
	inline glm::vec3& getRotation() { return m_Rotation; }
	inline glm::vec3& getScale()	{ return m_Scale; }

	inline const glm::mat4& getModelMatrix() { return m_ModelMatrix; }

	inline void Translate(const glm::vec3& vec) { m_Position += vec; }
	inline void Rotate(const glm::vec3& vec) { m_Rotation += vec; }

	inline void update()
	{
		m_ModelMatrix =
			glm::translate(m_Position) *
			glm::rotate(m_Rotation.z, glm::vec3(0, 0, 1)) *
			glm::rotate(m_Rotation.y, glm::vec3(0, 1, 0)) *
			glm::rotate(m_Rotation.x, glm::vec3(1, 0, 0)) *
			glm::scale(m_Scale);
	}
};

#endif // _TRANSFORM_H
