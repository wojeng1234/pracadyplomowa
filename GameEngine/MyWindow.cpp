#include "MyWindow.h"
#include "GlobalWndProc.h"


MyWindow::MyWindow()
{
}


MyWindow::~MyWindow()
{
	DestroyWindow(m_hWnd);
	UnregisterClass(m_Name.c_str(), m_Hinstance);
}

void MyWindow::init(const std::wstring& name, int x, int y, int width, int height, HINSTANCE hInstance, Engine* engine)
{
	m_Name = name;
	m_x = x;
	m_y = y;
	m_width = width;
	m_height = height;
	m_Hinstance = hInstance;
	m_pEngine = engine;

	WNDCLASS wc;
	ZeroMemory(&wc, sizeof(WNDCLASS));
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hIcon = LoadIcon(0, IDI_APPLICATION);
	wc.hInstance = hInstance;
	wc.lpfnWndProc = WndProc;
	//wc.lpfnWndProc = DefWindowProc;
	wc.lpszClassName = m_Name.c_str();
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	if (!RegisterClass(&wc))
	{
		MessageBox(0, L"Nie zarejestrowano klasy okna!", 0, 0);
		return;
	}

	m_hWnd = CreateWindow(
		m_Name.c_str(),
		m_Name.c_str(),
		//WS_OVERLAPPEDWINDOW,
		//WS_POPUP | WS_VISIBLE | WS_SYSMENU,
		WS_BORDER | WS_SIZEBOX | WS_MAXIMIZEBOX | WS_MINIMIZEBOX,
		x, y, width, height,
		0, 0,
		hInstance,
		0);

	if (!m_hWnd)
	{
		MessageBox(0, L"Nie utworzono okna!", 0, 0);
		return;
	}

	ShowWindow(m_hWnd, SW_SHOW);
	UpdateWindow(m_hWnd);
}