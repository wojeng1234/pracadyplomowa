#ifndef _HELPERS_H
#define _HELPERS_H

#include <string>
#include <vector>

bool BothAreSpaces(char lhs, char rhs);
std::vector<std::string> split(const std::string& s, char seperator);
std::string UnicodeToAscii(const std::wstring& wstr);
std::wstring AsciiToUnicode(const std::string& str);

#endif