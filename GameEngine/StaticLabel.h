#ifndef _STATIC_LABEL_H
#define _STATIC_LABEL_H

#include <string>
#include <vector>
#include <glm/glm.hpp>
#include "FontAtlas.h"
#include "Character.h"
#include "Mesh.h"
#include "Shader.h"
#include "Texture.h"
#include "Transform.h"

#ifndef FONT_TEX_SIZE
	#define FONT_TEX_SIZE 512
#endif

class StaticLabel
{
	FontAtlas* m_pFontAtlas;
	Mesh* m_pMesh;
	Texture* m_pTexture;
	Shader* m_pShader;
	Transform m_Transform;
	glm::vec3 m_Color;
	size_t m_NumIndicies;
	float m_Scale;


public:
	StaticLabel();
	~StaticLabel();

	void Create(FontAtlas* atlas, const std::wstring& label, const glm::vec2& translation, const glm::vec3& color, float scale = 1.0f);

	inline FontAtlas* getFontAtlas() { return m_pFontAtlas; }
	inline Mesh* getMesh() { return m_pMesh; }
	inline Shader* getShader() { return m_pShader; }
	inline Texture* getTexture() { return m_pTexture; }
	inline size_t getNumIndicies() { return m_NumIndicies; }

	inline glm::vec3& getColor() { return m_Color; }
	inline Transform& getTransform() { return m_Transform; }
	inline float getScale() { return m_Scale; }

	inline void SetShader(Shader* pShader) { m_pShader = pShader; }
	inline void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }
};

#endif