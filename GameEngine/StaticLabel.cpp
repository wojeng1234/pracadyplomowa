#include "StaticLabel.h"

StaticLabel::StaticLabel()
{
}


StaticLabel::~StaticLabel()
{
}

void StaticLabel::Create(FontAtlas* atlas, const std::wstring& label, const glm::vec2& translation, const glm::vec3& color, float scale)
{
	std::vector<glm::vec2> positions;
	std::vector<glm::vec2> coords;
	std::vector<GLushort>  indicies;

	m_pFontAtlas = atlas;

	m_Color = color;
	m_Transform.setPosition(glm::vec3(translation, 0.0f));
	m_Transform.setScale(glm::vec3(scale, -scale, 0.0f));
	m_Transform.update();
	//m_Translation = translation;
	m_Scale = scale;

	float xPos = 0.0f;

	for (auto c : label)
	{
		Character letter = atlas->GetCharacter(c);
		float width = letter.GetWidth();// *m_Scale;
		float height = letter.GetHeight();
		float xoffset = letter.GetXoffset();
		float yoffset = letter.GetYoffset();

		positions.push_back(glm::vec2((xPos + xoffset),(yoffset)));
		positions.push_back(glm::vec2((xPos + xoffset + width),(yoffset)));
		positions.push_back(glm::vec2((xPos + xoffset + width),(yoffset + height)));
		positions.push_back(glm::vec2((xPos + xoffset),(yoffset + height)));
		xPos += letter.GetXadvance();// *m_Scale;

		/*m_Positions.push_back(glm::vec2(2 * ((xPos + xoffset) / SCR_WIDTH) - 1, -2 * ((yoffset) / SCR_HEIGHT) + 1));
		m_Positions.push_back(glm::vec2(2 * ((xPos + xoffset + width) / SCR_WIDTH) - 1, -2 * ((yoffset) / SCR_HEIGHT) + 1));
		m_Positions.push_back(glm::vec2(2 * ((xPos + xoffset + width) / SCR_WIDTH) - 1, -2 * ((yoffset + height) / SCR_HEIGHT) + 1));
		m_Positions.push_back(glm::vec2(2 * ((xPos + xoffset) / SCR_WIDTH) - 1, -2 * ((yoffset + height) / SCR_HEIGHT) + 1));
		xPos += letter.GetXadvance() * m_Scale;*/

		coords.push_back(glm::vec2((letter.GetX()) / FONT_TEX_SIZE, 1 - (letter.GetY()) / FONT_TEX_SIZE));
		coords.push_back(glm::vec2((letter.GetX() + letter.GetWidth()) / FONT_TEX_SIZE, 1 - (letter.GetY()) / FONT_TEX_SIZE));
		coords.push_back(glm::vec2((letter.GetX() + letter.GetWidth()) / FONT_TEX_SIZE, 1 - (letter.GetY() + letter.GetHeight()) / FONT_TEX_SIZE));
		coords.push_back(glm::vec2((letter.GetX()) / FONT_TEX_SIZE, 1 - (letter.GetY() + letter.GetHeight()) / FONT_TEX_SIZE));

	}

	indicies.resize(6 * (positions.size() / 4));
	for (int i = 0; i < positions.size() / 4; ++i)
	{
		indicies[0 + i * 6] = 0 + i * 4;
		indicies[1 + i * 6] = 2 + i * 4;
		indicies[2 + i * 6] = 1 + i * 4;
		indicies[3 + i * 6] = 0 + i * 4;
		indicies[4 + i * 6] = 3 + i * 4;
		indicies[5 + i * 6] = 2 + i * 4;
	}

	m_pMesh = new Mesh();
	m_pMesh->LoadLabel(positions, coords, indicies);
}