#ifndef _TEXTURE_H
#define _TEXTURE_H

#include "Bitmap.h"
#include <GL/glew.h>
#include <string>


class Texture
{
private:
	Bitmap bitmap;
	GLuint textureID;
	GLenum channels;

public:
	Texture();
	~Texture();

	void load(const std::string &path);
	void update();
	inline void bind()					{ glBindTexture(GL_TEXTURE_2D, textureID); }
	inline void unBind()				{ glBindTexture(GL_TEXTURE_2D, 0); }
	inline Bitmap* getBitmapPointer()	{ return &bitmap; }
};

#endif
