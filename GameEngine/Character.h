#ifndef _CHARACTER_H
#define _CHARACTER_H

#include <glm/glm.hpp>

#ifndef FONT_TEX_SIZE
	#define FONT_TEX_SIZE 512
#endif

class Character
{

	float m_x;
	float m_y;
	float m_width;
	float m_height;
	float m_xoffset;
	float m_yoffset;
	float m_xadvance;
	wchar_t m_id;

	int m_PosInBuffer;

public:
	Character(wchar_t id, float x, float y, float w, float h, float xos, float yos, float xadv);
	Character() {}
	~Character();

	inline float GetX() { return m_x; }
	inline float GetY() { return m_y; }
	inline float GetWidth() { return m_width; }
	inline float GetHeight() { return m_height; }
	inline float GetXoffset() { return m_xoffset; }
	inline float GetYoffset() { return m_yoffset; }
	inline float GetXadvance() { return m_xadvance; }
	inline wchar_t GetID() { return m_id; }

	inline int GetPosInBuff() { return m_PosInBuffer; }
	inline void SetPosInBuff(int pib) { m_PosInBuffer = pib; }
};

#endif
