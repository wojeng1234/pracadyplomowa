#ifndef _STATIC_MODEL_3D_H
#define _STATIC_MODEL_3D_H

#include <string>
#include "Material.h"
#include "Mesh.h"
#include "Texture.h"
#include "Transform.h"

class StaticModel3D
{
	Transform m_Transform;
	std::string m_Name;
	Material* m_pMaterial;
	Mesh* m_pMesh;
	Texture* m_pTexture;

public:
	StaticModel3D() {}
	StaticModel3D(const std::string& name, Material* material, Mesh* mesh, Texture* texture, Transform transform);
	~StaticModel3D();

	inline const std::string& getName() { return m_Name; }
	inline Material* getMaterial() { return m_pMaterial; }
	inline Mesh* getMesh() { return m_pMesh; }
	inline Texture* getTexture() { return m_pTexture; }
	inline Transform& getTransform() { return m_Transform; }

};

#endif