#include "RenderingEngine.h"

#include <GL\wglew.h>


void RenderingEngine::AddCamera(Camera* camera)
{
	if (m_pCamera != NULL)
		delete m_pCamera;
	m_pCamera = camera;
}

void RenderingEngine::CreateTexture(const std::string& texName)
{
	if (m_Textures.find(texName) != m_Textures.end())
		return;

	Texture* temp = new Texture();
	temp->load(texName);
	m_Textures[texName] = temp;
}

void RenderingEngine::CreateShader(const std::string& shaderName, const SHADER_TYPE& shaderType)
{
	if (m_Shaders.find(shaderType) != m_Shaders.end())
		return;

	Shader* temp = new Shader();
	temp->createProgram(shaderName);
	m_Shaders[shaderType] = temp;
}

void RenderingEngine::CreateFontAtlas(const std::string& fontName)
{
	if (m_Fonts.find(fontName) != m_Fonts.end())
		return;

	FontAtlas* temp = new FontAtlas();
	temp->Load(fontName);
	m_Textures[(std::string(fontName) + ".png")] = temp->getTexture();
	m_Fonts[fontName] = temp;
}

void RenderingEngine::CreateStaticLabel(
	const std::string& fontName,
	const std::wstring& label,
	const glm::vec2& translation,
	const glm::vec3& color,
	float scale)
{
	if (m_Fonts.find(fontName) == m_Fonts.end())
		CreateFontAtlas(fontName);

	if (m_Shaders.find(SHADER_TYPE::FONT) == m_Shaders.end())
		CreateShader("font", SHADER_TYPE::FONT);

	StaticLabel* temp = new StaticLabel();
	temp->Create(m_Fonts[fontName], label, translation, color, scale);
	temp->SetShader(m_Shaders[SHADER_TYPE::FONT]);
	temp->SetTexture(m_Textures[(std::string(fontName) + ".png")]);

	std::string labelId = "Label";
	labelId += std::to_string(m_Labes.size());
	m_Meshes[labelId] = temp->getMesh();

	m_Labes.push_back(temp);
}

void RenderingEngine::CreateStaticModel3d(
	const std::string& modelName, 
	const std::string& meshName, 
	const std::string& materialName, 
	const std::string& textureName,
	Transform transform)
{
	/*if (m_StaticModels.find(modelName) != m_StaticModels.end())
		return m_StaticModels[modelName];*/

	CreateStaticMesh(meshName);
	CreateMaterial(materialName, MATERIAL_TYPE::MAT);
	CreateTexture(textureName);

	StaticModel3D* temp = new StaticModel3D(
		modelName,
		m_Materials[MATERIAL_TYPE::MAT],
		m_Meshes[meshName],
		m_Textures[textureName],
		transform);
	
	m_StaticModels[modelName] = temp;

	//return temp;
}

void RenderingEngine::CreateStaticMesh(const std::string& meshName)
{
	if (m_Meshes.find(meshName) != m_Meshes.end())
		return;

	Mesh* temp = new Mesh();
	temp->LoadFromObj(meshName);
	m_Meshes[meshName] = temp;
}

void RenderingEngine::CreateStaticGlassModel3D(
	const std::string& modelName, 
	const std::string& meshName,
	const glm::vec4& color,
	const Transform& transform)
{
	if (m_StaticGlassModels.find(modelName) != m_StaticGlassModels.end())
		return;

	CreateStaticMesh(meshName);

	StaticGlassModel3D* temp = new StaticGlassModel3D(
		modelName,
		m_Meshes[meshName],
		color,
		transform);

	m_StaticGlassModels[modelName] = temp;
}

void RenderingEngine::CreateMaterial(const std::string& materialName, const MATERIAL_TYPE& materialType)
{
	if (m_Materials.find(materialType) != m_Materials.end())
		return;

	Material* temp = new Material();
	temp->LoadFromFile(materialName);
	m_Materials[materialType] = temp;
}

void RenderingEngine::AddUniformForShader(SHADER_TYPE shaderType, const std::string &uniformName, UNIFORM uniformEnum)
{
	m_Shaders[shaderType]->addUniform(uniformName, uniformEnum);
}

void RenderingEngine::CreateDirectionalLight(
	const glm::vec4 &ambient,
	const glm::vec4 &diffuse,
	const glm::vec4 &specular,
	const glm::vec3 &direction)
{
	DirectionalLight* temp = new DirectionalLight(
		ambient, diffuse, specular, direction);
	temp->m_Type = LIGHT_TYPE::DIR;
	m_DirectionalLights.push_back(temp);
	m_Lights.push_back(temp);
}

void RenderingEngine::CreatePointLight(
	const glm::vec4 &ambient,
	const glm::vec4 &diffuse,
	const glm::vec4 &specular,
	const glm::vec3 &pos,
	const float &range,
	const glm::vec3 &att)
{
	PointLight* temp = new PointLight(
		ambient, diffuse, specular, pos, range, att);
	temp->m_Type = LIGHT_TYPE::POIN;
	m_PointLights.push_back(temp);
	m_Lights.push_back(temp);
}

void RenderingEngine::CreateSpotLight(
	const glm::vec4 &ambient,
	const glm::vec4 &diffuse,
	const glm::vec4 &specular,
	const glm::vec3 &pos,
	const float &range,
	const glm::vec3 &dir,
	const float &spot,
	const glm::vec3 &att)
{
	SpotLight* temp = new SpotLight(
		ambient, diffuse, specular, pos, range, dir, spot, att);
	temp->m_Type = LIGHT_TYPE::SPOT;
	m_SpotLights.push_back(temp);
	m_Lights.push_back(temp);
}

//Shader* RenderingEngine::GetShader(const SHADER_TYPE& shader)
//{
//	if (m_Shaders.find(shader) == m_Shaders.end())
//		return nullptr;
//	else
//		return m_Shaders[shader];
//}