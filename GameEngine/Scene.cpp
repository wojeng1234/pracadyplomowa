#include "Scene.h"

#include <iostream>
#include <Windows.h>
#include "Engine.h"
#include "helpers.h"
#include "tinyxml\tinyxml.h"

extern Engine* gEngine;

Scene::Scene()
{
}


Scene::~Scene()
{
}

bool Scene::Load(const std::string& sceneName)
{
	TiXmlDocument doc((std::string("..\\resourecs\\levels\\") + sceneName + ".xml").c_str());
	doc.LoadFile();

	TiXmlElement* root = doc.FirstChildElement("level");
	if (!root) 
		return false;

	gEngine->getRenderingEngine()->ClearScene();

	//Object
	TiXmlElement* object = root->FirstChildElement("object");
	while (object)
	{
		TiXmlElement* transform = object->FirstChildElement("transform");
		TiXmlElement* pos = transform->FirstChildElement("position");
		TiXmlElement* rot = transform->FirstChildElement("rotation");
		TiXmlElement* scale = transform->FirstChildElement("scale");

		std::string type = object->Attribute("type");
		if (type == "static_model")
		{
			TiXmlElement* mesh = object->FirstChildElement("mesh");
			TiXmlElement* texture = object->FirstChildElement("texture");
			TiXmlElement* material = object->FirstChildElement("material");


			gEngine->getRenderingEngine()->CreateStaticModel3d(
				object->Attribute("name"),
				mesh->Attribute("name"),
				material->Attribute("name"),
				texture->Attribute("name"),
				Transform(
					glm::vec3(atof(pos->Attribute("x")), atof(pos->Attribute("y")), atof(pos->Attribute("z"))),
					glm::vec3(atof(rot->Attribute("x")), atof(rot->Attribute("y")), atof(rot->Attribute("z"))),
					glm::vec3(atof(scale->Attribute("x")), atof(scale->Attribute("y")), atof(scale->Attribute("z")))
				)
			);
		}
		else if (type == "static_glass")
		{
			TiXmlElement* mesh = object->FirstChildElement("mesh");
			TiXmlElement* color = object->FirstChildElement("color");


			gEngine->getRenderingEngine()->CreateStaticGlassModel3D(
				object->Attribute("name"),
				mesh->Attribute("name"),
				glm::vec4(atof(color->Attribute("r")), atof(color->Attribute("g")), atof(color->Attribute("b")), atof(color->Attribute("a"))),
				Transform(
					glm::vec3(atof(pos->Attribute("x")), atof(pos->Attribute("y")), atof(pos->Attribute("z"))),
					glm::vec3(atof(rot->Attribute("x")), atof(rot->Attribute("y")), atof(rot->Attribute("z"))),
					glm::vec3(atof(scale->Attribute("x")), atof(scale->Attribute("y")), atof(scale->Attribute("z")))
				)
			);
		}

		object = object->NextSiblingElement("object");
		
	}

	//Text
	TiXmlElement* label = root->FirstChildElement("label");
	while (label)
	{
		TiXmlElement* text = label->FirstChildElement("text");
		TiXmlElement* position = label->FirstChildElement("position");
		TiXmlElement* color = label->FirstChildElement("color");

		gEngine->getRenderingEngine()->CreateStaticLabel(
			text->Attribute("font"),
			AsciiToUnicode(text->Attribute("value")),
			glm::vec2(atof(position->Attribute("x")), atof(position->Attribute("y"))),
			glm::vec3(atof(color->Attribute("r")), atof(color->Attribute("g")), atof(color->Attribute("b"))),
			atof(text->Attribute("size"))
			);

		label = label->NextSiblingElement("label");

	}

	TiXmlElement* directional_light = root->FirstChildElement("directional_light");
	while (directional_light)
	{
		TiXmlElement* ambient = directional_light->FirstChildElement("ambient");
		TiXmlElement* diffuse = directional_light->FirstChildElement("diffuse");
		TiXmlElement* specular = directional_light->FirstChildElement("specular");
		TiXmlElement* direction = directional_light->FirstChildElement("direction");

		gEngine->getRenderingEngine()->CreateDirectionalLight(
			glm::vec4(atof(ambient->Attribute("r")), atof(ambient->Attribute("g")), atof(ambient->Attribute("b")), 1.0f),
			glm::vec4(atof(diffuse->Attribute("r")), atof(diffuse->Attribute("g")), atof(diffuse->Attribute("b")), 1.0f),
			glm::vec4(atof(specular->Attribute("r")), atof(specular->Attribute("g")), atof(specular->Attribute("b")), atof(specular->Attribute("s"))),
			glm::vec3(atof(direction->Attribute("x")), atof(direction->Attribute("y")), atof(direction->Attribute("z")))
			);

		directional_light = directional_light->NextSiblingElement("directional_light");
	}

	TiXmlElement* point_light = root->FirstChildElement("point_light");
	while (point_light)
	{
		TiXmlElement* ambient = point_light->FirstChildElement("ambient");
		TiXmlElement* diffuse = point_light->FirstChildElement("diffuse");
		TiXmlElement* specular = point_light->FirstChildElement("specular");
		TiXmlElement* position = point_light->FirstChildElement("position");
		TiXmlElement* attenuation = point_light->FirstChildElement("attenuation");

		gEngine->getRenderingEngine()->CreatePointLight(
			glm::vec4(atof(ambient->Attribute("r")), atof(ambient->Attribute("g")), atof(ambient->Attribute("b")), 1.0f),
			glm::vec4(atof(diffuse->Attribute("r")), atof(diffuse->Attribute("g")), atof(diffuse->Attribute("b")), 1.0f),
			glm::vec4(atof(specular->Attribute("r")), atof(specular->Attribute("g")), atof(specular->Attribute("b")), atof(specular->Attribute("s"))),
			glm::vec3(atof(position->Attribute("x")), atof(position->Attribute("y")), atof(position->Attribute("z"))),
			atof(position->Attribute("range")),
			glm::vec3(atof(attenuation->Attribute("r")), atof(attenuation->Attribute("g")), atof(attenuation->Attribute("b")))
			);

		point_light = point_light->NextSiblingElement("point_light");
	}

	TiXmlElement* spot_light = root->FirstChildElement("spot_light");
	while (spot_light)
	{
		TiXmlElement* ambient = spot_light->FirstChildElement("ambient");
		TiXmlElement* diffuse = spot_light->FirstChildElement("diffuse");
		TiXmlElement* specular = spot_light->FirstChildElement("specular");
		TiXmlElement* position = spot_light->FirstChildElement("position");
		TiXmlElement* direction = spot_light->FirstChildElement("direction");
		TiXmlElement* attenuation = spot_light->FirstChildElement("attenuation");

		gEngine->getRenderingEngine()->CreateSpotLight(
			glm::vec4(atof(ambient->Attribute("r")), atof(ambient->Attribute("g")), atof(ambient->Attribute("b")), 1.0f),
			glm::vec4(atof(diffuse->Attribute("r")), atof(diffuse->Attribute("g")), atof(diffuse->Attribute("b")), 1.0f),
			glm::vec4(atof(specular->Attribute("r")), atof(specular->Attribute("g")), atof(specular->Attribute("b")), atof(specular->Attribute("s"))),
			glm::vec3(atof(position->Attribute("x")), atof(position->Attribute("y")), atof(position->Attribute("z"))),
			atof(position->Attribute("range")),
			glm::vec3(atof(direction->Attribute("x")), atof(direction->Attribute("y")), atof(direction->Attribute("z"))),
			atof(direction->Attribute("spot")),
			glm::vec3(atof(attenuation->Attribute("r")), atof(attenuation->Attribute("g")), atof(attenuation->Attribute("b")))
			);

		spot_light = spot_light->NextSiblingElement("spot_light");
	}

	return true;
}