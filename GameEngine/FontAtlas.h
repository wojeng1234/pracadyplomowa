#ifndef _FONT_ATLAS_H
#define _FONT_ATLAS_H

#include <string>
#include <map>
#include <GL/glew.h>
#include <vector>
#include "Texture.h"
#include "Character.h"

#ifndef FONT_TEX_SIZE
	#define FONT_TEX_SIZE 512
#endif

//Temporary
#ifndef SCR_WIDTH
#define SCR_WIDTH 1280
#endif
#ifndef SCR_HEIGHT
#define SCR_HEIGHT 960
#endif

enum INDEX
{
	ID = 1,
	X, Y, WIDTH, HEIGHT, XOFFSET, YOFFSET, XADVANCE
};

typedef std::map<wchar_t, Character> CharacterMap;

class FontAtlas
{
	Texture* m_pTexture;
	CharacterMap m_CharMap;
	std::string m_Name;
	int m_NumChars;

	///////////////////////////////OpenGL
	GLuint m_vaoID;
	GLuint m_vboPositionsID;
	GLuint m_vboCoordsID;
	//GLuint m_vboIndiciesID;
	std::vector<GLushort> m_Indicies;
	///////////////////////////////OpenGL

	void GenFontForDynamicLabel();

public:
	FontAtlas();
	~FontAtlas();

	bool Load(const std::string& fontName);
	Character& GetCharacter(wchar_t c);
	inline Texture* getTexture() { return m_pTexture; }
	inline GLuint GetVAO() { return m_vaoID; }

	//Only after Load call!
	inline GLushort* getIndicies() { return &m_Indicies[0]; }
};

#endif
