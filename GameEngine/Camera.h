#pragma once

#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp>

class Camera
{
public:

	glm::mat4 viewMatrix;
	glm::mat4 perspectiveProjectionMatrix;
	glm::mat4 orthogolanProjectionMatrix;

	glm::vec3 up;
	glm::vec3 right;
	glm::vec3 look;
	glm::vec3 position;

	glm::vec3 translation;
	float yaw, pitch, roll;
	float rotSpeed;

	virtual void rotate(float yaw, float pitch, float roll) = 0;
	virtual void update() = 0;

	inline void setPerspectiveProjectionMatrix(float fov, float aspect, float zNear, float zFar)
	{
		perspectiveProjectionMatrix = glm::perspective(fov, aspect, zNear, zFar);
	}

	inline void setOrthogonalProjectionMatrix(float left, float right, float bottom, float top, float zNear, float zFar)
	{
		orthogolanProjectionMatrix = glm::ortho(left, right, bottom, top, zNear, zFar);
	}

	inline glm::mat4& getProjectionMatrix()
	{
		return perspectiveProjectionMatrix;
		//return orthogolanProjectionMatrix;
	}

	inline glm::mat4& getViewMatrix()
	{
		return viewMatrix;
	}

	inline glm::vec3& getUp()		{ return up; }
	inline glm::vec3& getRight()	{ return right; }
	inline glm::vec3& getLook()		{ return look; }
	inline glm::vec3& getPos()		{ return position; }
	inline float getYaw()			{ return yaw; }
	inline float getPitch()			{ return pitch; }
	inline float getRoll()			{ return roll; }

};
