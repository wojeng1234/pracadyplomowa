#ifndef _MATERIAL_H
#define _MATERIAL_H 

#include <string>
#include <glm/glm.hpp>

class Material
{
	glm::vec4 m_Ambient;
	glm::vec4 m_Diffuse;
	glm::vec4 m_Specular;
	float m_Shinnes;
	float m_Transparency;

public:
	Material();
	Material(
		const glm::vec3& ambient,
		const glm::vec3& diffuse,
		const glm::vec3& specular,
		float shinnes);
	~Material();

	bool LoadFromFile(const std::string& materialName);

	inline void SetAmbient(const glm::vec3& a) { m_Ambient = glm::vec4(a, 1.0f); }
	inline void SetDiffuse(const glm::vec3& d) { m_Diffuse = glm::vec4(d, 1.0f); }
	inline void SetSepcukar(const glm::vec3& s) { m_Specular = glm::vec4(s, m_Shinnes); }
	inline void SetShinnes(float s) { m_Shinnes = s; }
	inline void SetTransparency(float t) { m_Transparency = t; }

	inline glm::vec4& GetAmbient() { return m_Ambient; }
	inline glm::vec4& GetDiffuse() { return m_Diffuse; }
	inline glm::vec4& GetSpecular() { return m_Specular; }
	inline float GetShinines() { return m_Shinnes; }
	inline float GetTransparency() { return m_Transparency; }
};

#endif // _MAATERIAL_H
