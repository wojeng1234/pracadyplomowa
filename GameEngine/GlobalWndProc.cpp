#include "GlobalWndProc.h"
#include <GL\glew.h>
#include <sstream>
#include "Engine.h"

extern Engine* gEngine;

LPARAM CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_LBUTTONDOWN:
		gEngine->getInput()->setLeftButtonDonw();
		return 0;

	case WM_LBUTTONUP:
		gEngine->getInput()->setLeftButtonUp();
		return 0;

	case WM_RBUTTONDOWN:
		gEngine->getInput()->setRightButtonDonw();
		return 0;

	case WM_RBUTTONUP:
		gEngine->getInput()->setRightButtonUp();
		return 0;

	case WM_MOUSEMOVE:
		gEngine->getInput()->setXWndPos(LOWORD(lParam));
		gEngine->getInput()->setYWndPos(HIWORD(lParam));
		return 0;

	case WM_KEYDOWN:
		gEngine->getInput()->setKeyDown(wParam);
		return 0;

	case WM_KEYUP:
		gEngine->getInput()->setKeyUp(wParam);
		return 0;

	case WM_SIZE:
		gEngine->getWindow()->setWidth(LOWORD(lParam));
		gEngine->getWindow()->setHeight(HIWORD(lParam));
		glViewport(0, 0, 
			gEngine->getWindow()->getWidth(), 
			gEngine->getWindow()->getHeight());
		OutputDebugString(L"Window procedure -> Size\n");
		return 0;

	case WM_EXITSIZEMOVE:
		gEngine->getRenderingEngine()->getOwnCamera()->setPerspectiveProjectionMatrix(
			70.0f,
			(float)gEngine->getWindow()->getWidth() / (float)gEngine->getWindow()->getHeight(),
			0.1f, 1000.0f);
		gEngine->getRenderingEngine()->getOwnCamera()->setOrthogonalProjectionMatrix(
			-(float)gEngine->getWindow()->getWidth() / 200,
			(float)gEngine->getWindow()->getWidth() / 200,
			-(float)gEngine->getWindow()->getHeight() / 200,
			(float)gEngine->getWindow()->getHeight() / 200,
			-100.0f, 100.0f
			);
		OutputDebugString(L"Window procedure -> Exit size move\n");
		return 0;

	case WM_DESTROY:
		//MessageBox(0, L"Zamykam", 0, 0);
		std::cout << "Zamykam\n";
		PostQuitMessage(0);
		return 0;

	default:
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
}