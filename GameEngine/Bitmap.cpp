#include "Bitmap.h"

#include "soil/SOIL.h"

Bitmap::Bitmap()
{
}


Bitmap::~Bitmap()
{
	SOIL_free_image_data(data);
}

void Bitmap::loadFromFile(const std::string &fileName)
{
	data = SOIL_load_image((std::string("../resourecs/textures/") + fileName).c_str(), &width, &height, &channels, SOIL_LOAD_AUTO);

	if (!data)
	{
		//MessageBoxA(0, fileName.c_str(), "Nie odnaleziono tekstury", 0);
		data = SOIL_load_image("../resourecs/textures/Error128x128.png", &width, &height, &channels, SOIL_LOAD_AUTO);
	}

	/*Odwracanie bitmapy na osi x*/
	int i, j;
	for (j = 0; j * 2 < height; ++j)
	{
		int index1 = j * width * channels;
		int index2 = (height - 1 - j) * width * channels;
		for (i = width * channels; i > 0; --i)
		{
			unsigned char temp = data[index1];
			data[index1] = data[index2];
			data[index2] = temp;
			++index1;
			++index2;
		}
	}
}
