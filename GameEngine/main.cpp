#include <algorithm>
#include <thread>
#include "MyWindow.h"
#include "RenderingEngine.h"
#include "Texture.h"
#include "Mesh.h"
#include "FontAtlas.h"
#include "StaticLabel.h"
#include <glm/glm.hpp>
#include "Timer.h"
#include "Engine.h"
#include "Input.h"
#include "FreeCamera.h"


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pCmdLine, int nShowCmd);

Engine* gEngine;
std::thread appThread;

std::string levelToLoad;
bool isLoadLevelCalled;

int main( int _argc, char* _argv[] )
{
	SetConsoleTitle(L"Praca dyplomowa");
	isLoadLevelCalled = false;

	//appThread = std::thread(WinMain, (HINSTANCE)GetModuleHandle(NULL), (HINSTANCE)0, (LPSTR)0, SW_SHOW);
    appThread = std::thread( AppMain, _argc, _argv );
	
	std::cout << "Witaj!" << std::endl;
	std::cout << "Aby dowiedziec sie wiecej wpisz komende: \'help\'" << std::endl;
	std::string command;
	while (1)
	{
		std::cin >> command;
		std::transform(command.begin(), command.end(), command.begin(), ::tolower);

		if (command == "load")
		{
			std::cout << "Prosze podac nazwe sceny:" << std::endl << "\t >> ";
			std::cin >> levelToLoad;
			isLoadLevelCalled = true;
		}

		else if (command == "quit")
		{
			SendMessageA(gEngine->getWindow()->getHwnd(), WM_DESTROY, 0, 0);
			break;
		}

		else if (command == "help")
		{
			std::cout << "Komendy:" << std::endl;
			std::cout << "\tload - laduje wybrana scene" << std::endl;
			std::cout << "\tquit - konczy prace programu" << std::endl;
			std::cout << "Sterowanie po scenie 3D:" << std::endl;
			std::cout << "\tX - wl/wyl rozgladanie po scenie" << std::endl;
			std::cout << "\tW/S/A/D - poruszanie sie przod/tyl/lewo/prawo" << std::endl;
			std::cout << "\tQ/E - przemieszczanie kamery gora/dol" << std::endl;
			std::cout << "\tLEFT SHIFT - przemieszczanie kamery szybko/wolno" << std::endl;
		}

		else
		{
			std::cout << "Nie ma takiej komendy." << std::endl;
		}
	}

	appThread.join();

	return 0;
}

//int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pCmdLine, int nShowCmd)
int AppMain( int _argc, char* _argv[] )
{
	gEngine = new Engine();
	gEngine->init(new MyWindow(), new EngineTimer(), new RenderingEngine(), new Input(), hInstance);

	MSG msg = { 0 };
	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			DispatchMessage(&msg);
			TranslateMessage(&msg);
		}

		if (isLoadLevelCalled)
		{
			if (gEngine->getScene()->Load(levelToLoad))
				std::cout << "Scene zaladowano pomyslnie." << std::endl;
			else
				std::cout << "Blad ladowania sceny." << std::endl;
			isLoadLevelCalled = false;
		}

		gEngine->getEngineTimer()->tick();

		gEngine->ProcessRender();

		gEngine->ProcessInput();

		gEngine->getInput()->update();
	}

	delete gEngine;

	return msg.lParam;
}
