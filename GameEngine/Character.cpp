#include "Character.h"

Character::Character(wchar_t id, float x, float y, float w, float h, float xos, float yos, float xadv)
:
m_id(id),
m_x(x),
m_y(y),
m_width(w),
m_height(h),
m_xoffset(xos),
m_yoffset(yos),
m_xadvance(xadv)
{
}


Character::~Character()
{
}
