#include "Input.h"
#include <Windows.h>

Input::Input()
{
	ZeroMemory(this, sizeof(Input));
	SCREEN_WIDTH_HALF = GetSystemMetrics(SM_CXSCREEN) / 2;
	SCREEN_HEIGHT_HALF = GetSystemMetrics(SM_CYSCREEN) / 2;
}

Input::~Input() {}

void Input::update()
{
	if (isCursorLocked)
	{
		getCursorPos(xScrPos, yScrPos);
		setXYDelta(xScrPos - SCREEN_WIDTH_HALF, yScrPos - SCREEN_HEIGHT_HALF);
		setCursorPos(SCREEN_WIDTH_HALF, SCREEN_HEIGHT_HALF);
	}
	else
	{
		int x, y;
		getCursorPos(x, y);
		setXYDelta(x - xScrPos, y - yScrPos);
		setXYPos(x, y);
	}

	for (int i = 0; i < 256; ++i)
		keysDown[i] = keysUp[i] = false;
	LeftButtonDown = LeftButtonUp = RightButtonDown = RightButtonUp = false;
	

	//XInputGetState(0, &gamePadState); //UserIndex jest r�wne 0 poniewarz obs�ugujemy tylko 1 pad
}