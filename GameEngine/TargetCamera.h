#pragma once

#include "Camera.h"
#include <algorithm>

class TargetCamera : public Camera
{
private:
	glm::vec3 target;

	float minRy, maxRy;
	float distance;
	float minDistance, maxDistance;

public:
	TargetCamera();
	~TargetCamera();

	inline void setTarget(const glm::vec3 &t)
	{
		target = t;
		distance = glm::distance(position, target);
		distance = std::max(minDistance, std::min(distance, maxDistance));
	}

	inline glm::vec3& getTarget()
	{
		return target;
	}

	inline void rotate(float yaw, float pitch, float roll)
	{
		this->yaw = yaw;
		this->pitch = std::min(std::max(pitch, minRy), maxRy);
		this->roll = roll;
	}

	void update();

	inline void pan(const float dx, const float dy)
	{
		glm::vec3 X = right*dx;
		glm::vec3 Y = up*dy;
		position += X + Y;
		target += X + Y;
		update();
	}


	inline void zoom(const float amount)
	{
		position += look * amount;
		distance = glm::distance(position, target);
		distance = std::max(minDistance, std::min(distance, maxDistance));
		update();
	}

	inline void move(const float dx, const float dy)
	{
		glm::vec3 X = right*dx;
		glm::vec3 Y = look*dy;
		position += X + Y;
		target += X + Y;
		update();
	}
};