#ifndef _BITMAP_H
#define _BITMAP_H

#include <string>
#include "SOIL/SOIL.h"


class Bitmap
{
private:
	int width, height, channels;
	unsigned char* data;

public:
	Bitmap();
	~Bitmap();

	void loadFromFile(const std::string &fileName);

	unsigned char* getData()	{ return data; }
	int getWidth()				{ return width; }
	int getHeight()				{ return height; }
	int getChannelsNum()		{ return channels; }
};

#endif // _BITMAP_H
