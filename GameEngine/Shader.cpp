#include "Shader.h"

#include <sstream>
#include <fstream>
#include <Windows.h>

void LinkAndValidateProgram(GLuint program);
GLuint LoadShader(const GLenum type, const char* name);

Shader::Shader()
{
}


Shader::~Shader()
{
	glDeleteProgram(m_Program);
}

void Shader::createProgram(const std::string &name)
{
	GLuint vertex_shader, fragment_shader;

	m_Program = glCreateProgram();
	
	vertex_shader = LoadShader(GL_VERTEX_SHADER, (std::string("..\\resourecs\\shaders\\") + name + ".vertex").c_str());
	fragment_shader = LoadShader(GL_FRAGMENT_SHADER, (std::string("..\\resourecs\\shaders\\") + name + ".fragment").c_str());

	glAttachShader(m_Program, vertex_shader);
	glAttachShader(m_Program, fragment_shader);

	LinkAndValidateProgram(m_Program);

	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
}

void Shader::use()
{
	glUseProgram(m_Program);
}

void Shader::unUse()
{
	glUseProgram(0);
}

void Shader::addUniform(const std::string &uniformName, UNIFORM uniformEnum)
{
	GLuint uniform = glGetUniformLocation(m_Program, uniformName.c_str());
	if (uniform != -1)
		m_Uniforms[uniformEnum] = uniform;
	else
	{
		MessageBoxA(0, (std::string("Nie ma takiego uniformu ") + uniformName).c_str(), 0, 0);
		exit(0);
	}
}

GLuint LoadShader(const GLenum type, const char* name)
{
	std::wostringstream errorStream;
	std::ifstream file;
	file.open(name, std::ios::binary);

	if (file.bad())
	{
		errorStream << "Niepoprwny odczyt pliku shadera " << name << std::endl;
		MessageBox(0, errorStream.str().c_str(), 0, 0);
		exit(0);
	}

	file.seekg(0, std::ios::end);
	int len = static_cast<int>(file.tellg());

	if (len < 0)
	{
		errorStream << "Niepoprwny odczyt pliku shadera " << name << std::endl;
		MessageBox(0, errorStream.str().c_str(), 0, 0);
		exit(0);
	}

	file.seekg(std::ios::beg);
	GLchar* srcBuf = new GLchar[(len + 1) * sizeof(GLchar)];
	file.read(srcBuf, len);
	srcBuf[len] = NULL;
	file.close();

	GLuint shader = glCreateShader(type);

	glShaderSource(shader, 1, const_cast<const GLchar**>(&srcBuf), NULL);

	delete[] srcBuf;

	glCompileShader(shader);

	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE)
	{
		errorStream << "Niepoprawna kompilacja shadera " << name << std::endl;

		GLint logLength;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
		char* log = new char[logLength];
		glGetShaderInfoLog(shader, logLength, NULL, log);
		errorStream << log << std::endl;
		delete[] log;
		MessageBox(0, errorStream.str().c_str(), 0, 0);
		exit(0);
	}

	return shader;
}

void LinkAndValidateProgram(GLuint program)
{
	glLinkProgram(program);

	std::wostringstream errorStream;
	GLint status;

	glGetProgramiv(program, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		errorStream << "Niepoprawna konsolidacja programu." << std::endl;

		GLint logLength;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		char* log = new char[logLength];
		glGetProgramInfoLog(program, logLength, NULL, log);
		errorStream << log << std::endl;
		delete[] log;
		MessageBox(0, errorStream.str().c_str(), 0, 0);
		exit(0);
	}

	glValidateProgram(program);

	glGetProgramiv(program, GL_VALIDATE_STATUS, &status);
	if (status == GL_FALSE)
	{
		errorStream << "Niepoprawna walidacja programu." << std::endl;

		GLint logLength;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		char* log = new char[logLength];
		glGetProgramInfoLog(program, logLength, NULL, log);
		errorStream << log << std::endl;
		delete[] log;
		MessageBox(0, errorStream.str().c_str(), 0, 0);
		exit(0);
	}
}