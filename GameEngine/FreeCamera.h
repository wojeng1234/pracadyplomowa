#pragma once

#include "Camera.h"

class FreeCamera : public Camera
{
private:
	

public:
	FreeCamera();
	~FreeCamera();

	void update();

	inline void walk(float speed) 
	{
		translation += (look * speed);
	}

	inline void strafe(float speed)
	{
		translation += (right * speed);
	}

	inline void lift(float speed)
	{
		translation += (up * speed);
	}

	inline void rotate(float yaw, float pitch, float roll)
	{
		this->yaw = yaw;
		this->pitch = pitch;
		this->roll = roll;
	}

	inline void setTranslation(glm::vec3 &t) { translation = t; }

	inline void setRotSpeed(float rs) { rotSpeed = rs; }
};