#ifndef _LIGHTS_H
#define _LIGHTS_H

#include <glm/glm.hpp>

enum LIGHT_TYPE
{
	DIR,
	POIN,
	SPOT
};

class Light
{
public:
	LIGHT_TYPE m_Type;
};

class DirectionalLight : public Light
{
private:
	glm::vec4 m_Ambient;
	glm::vec4 m_Diffuse;
	glm::vec4 m_Specular;
	glm::vec3 m_Direction;

public:
	DirectionalLight(){}
	DirectionalLight(
		const glm::vec4 &ambient,
		const glm::vec4 &diffuse,
		const glm::vec4 &specular,
		const glm::vec3 &direction):
	m_Ambient(ambient), 
	m_Diffuse(diffuse),
	m_Specular(specular),
	m_Direction(direction)
	{}

	~DirectionalLight(){}

	inline glm::vec4& getAmbient() { return m_Ambient; }
	inline glm::vec4& getDiffuse() { return m_Diffuse; }
	inline glm::vec4& getSpecular() { return m_Specular; }
	inline glm::vec3& getDirection() { return m_Direction; }

	inline void setAmbient(const glm::vec4 &ambient) { m_Ambient = ambient; }
	inline void setDiffuse(const glm::vec4 &diffuse) { m_Diffuse = diffuse; }
	inline void setSpecular(const glm::vec4 &specular) { m_Specular = specular; }
	inline void setDirection(const glm::vec3 &pos) { m_Direction = pos; }
};

class PointLight : public Light
{
private:
	glm::vec4 m_Ambient;
	glm::vec4 m_Diffuse;
	glm::vec4 m_Specular;
	glm::vec3 m_Pos;
	glm::vec3 m_Att;
	float m_Range;

public:
	PointLight(){}
	PointLight(
		const glm::vec4 &ambient,
		const glm::vec4 &diffuse,
		const glm::vec4 &specular,
		const glm::vec3 &pos,
		const float &range,
		const glm::vec3 &att) :
		m_Ambient(ambient),
		m_Diffuse(diffuse),
		m_Specular(specular),
		m_Pos(pos),
		m_Range(range),
		m_Att(att)
	{}

	~PointLight(){}

	inline glm::vec4& getAmbient() { return m_Ambient; }
	inline glm::vec4& getDiffuse() { return m_Diffuse; }
	inline glm::vec4& getSpecular() { return m_Specular; }
	inline glm::vec3& getPos() { return m_Pos; }
	inline glm::vec3& getAtt() { return m_Att; }
	inline float& getRange() { return m_Range; }

	inline void setAmbient(const glm::vec4 &ambient) { m_Ambient = ambient; }
	inline void setDiffuse(const glm::vec4 &diffuse) { m_Diffuse = diffuse; }
	inline void setSpecular(const glm::vec4 &specular) { m_Specular = specular; }
	inline void setPos(const glm::vec3 &pos) { m_Pos = pos; }
	inline void setAtt(const glm::vec3 &att) { m_Att = att; }
	inline void setRange(const float& range) { m_Range = range; }
};

class SpotLight : public Light
{
private:
	glm::vec4 m_Ambient;
	glm::vec4 m_Diffuse;
	glm::vec4 m_Specular;
	glm::vec3 m_Pos;
	float m_Range;
	glm::vec3 m_Dir;
	float m_Spot;
	glm::vec3 m_Att;

public:
	SpotLight(){}
	SpotLight(
		const glm::vec4 &ambient,
		const glm::vec4 &diffuse,
		const glm::vec4 &specular,
		const glm::vec3 &pos,
		const float &range,
		const glm::vec3 &dir,
		const float &spot,
		const glm::vec3 &att) :
		m_Ambient(ambient),
		m_Diffuse(diffuse),
		m_Specular(specular),
		m_Pos(pos),
		m_Range(range),
		m_Dir(dir),
		m_Spot(spot),
		m_Att(att)
	{}

	~SpotLight(){}

	inline glm::vec4& getAmbient() { return m_Ambient; }
	inline glm::vec4& getDiffuse() { return m_Diffuse; }
	inline glm::vec4& getSpecular() { return m_Specular; }
	inline glm::vec3& getPos() { return m_Pos; }
	inline glm::vec3& getDir() { return m_Dir; }
	inline glm::vec3& getAtt() { return m_Att; }
	inline float& getRange() { return m_Range; }
	inline float& getSpot() { return m_Spot; }

	inline void setAmbient(const glm::vec4 &ambient) { m_Ambient = ambient; }
	inline void setDiffuse(const glm::vec4 &diffuse) { m_Diffuse = diffuse; }
	inline void setSpecular(const glm::vec4 &specular) { m_Specular = specular; }
	inline void setPos(const glm::vec3 &pos) { m_Pos = pos; }
	inline void setDir(const glm::vec3 &dir) { m_Dir = dir; }
	inline void setAtt(const glm::vec3 &att) { m_Att = att; }
	inline void setRange(const float& range) { m_Range = range; }
	inline void setSpot(const float& spot) { m_Spot = spot; }
};


#endif // _LIGHTS_H
