#ifndef _ENGINE_TIMER_H
#define _ENGINE_TIMER_H


class EngineTimer
{
public:
	EngineTimer();

	float totalTime() const;
	inline float deltaTime() const { return(float)mDeltaTime; }

	void reset();
	void start();
	void stop();
	void tick();

private:
	double mSecondsPerCount;
	double mDeltaTime;

	__int64 mBaseTime;
	__int64 mPausedTime;
	__int64 mStopTime;
	__int64 mPrevTime;
	__int64 mCurrTime;

	bool mStopped;
};

#endif // _ENGINE_TIMER_H
