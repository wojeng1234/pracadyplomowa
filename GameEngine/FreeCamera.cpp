#include "FreeCamera.h"

FreeCamera::FreeCamera()
{
	yaw = pitch = roll = 0.0f;
	rotSpeed = 1.0f;
}

FreeCamera::~FreeCamera()
{
}

void FreeCamera::update()
{
	glm::mat4 R = glm::yawPitchRoll(rotSpeed * yaw, rotSpeed * pitch, rotSpeed * roll);

	position += translation;
	translation = glm::vec3(0);

	look = glm::vec3(R*glm::vec4(0, 0, 1, 0));
	glm::vec3 tgt = position + look;
	up = glm::vec3(R*glm::vec4(0, 1, 0, 0));
	right = glm::cross(look, up);
	viewMatrix = glm::lookAt(position, tgt, up);
}