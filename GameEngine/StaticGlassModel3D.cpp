#include "StaticGlassModel3D.h"


StaticGlassModel3D::StaticGlassModel3D(const std::string& name, Mesh* mesh, const glm::vec4& color, const Transform& transform)
{
	m_Name = name;
	m_pMesh = mesh;
	m_Color = color;
	m_Transform = transform;
}

StaticGlassModel3D::StaticGlassModel3D()
{

}

StaticGlassModel3D::~StaticGlassModel3D()
{
}
