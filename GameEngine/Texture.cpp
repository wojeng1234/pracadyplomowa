#include "Texture.h"
#include <Windows.h>

Texture::Texture() {}

Texture::~Texture()
{
	glBindTexture(GL_TEXTURE_2D, 0);
	glDeleteTextures(1, &textureID);
}

void Texture::load(const std::string &path)
{
	bitmap.loadFromFile(path);

	glGenTextures(1, &textureID);
	//glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	switch (bitmap.getChannelsNum())
	{
	case 4: channels = GL_RGBA; break;
	case 3: channels = GL_RGB;  break;
	default: break;
	}

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bitmap.getWidth(), bitmap.getHeight(), 0, channels, GL_UNSIGNED_BYTE, bitmap.getData());
	//glGenerateMipmap(GL_TEXTURE_2D);
}

void Texture::update()
{
	bind();
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bitmap.getWidth(), bitmap.getHeight(), 0, channels, GL_UNSIGNED_BYTE, bitmap.getData());
	glGenerateMipmap(GL_TEXTURE_2D);
	unBind();
}