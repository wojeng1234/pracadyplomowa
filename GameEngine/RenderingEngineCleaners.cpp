#include "RenderingEngine.h"

void RenderingEngine::ClearScene()
{
	for (auto& smodel : m_StaticModels)
		delete smodel.second;
	m_StaticModels.clear();

	for (auto& sgmodel : m_StaticGlassModels)
		delete sgmodel.second;
	m_StaticGlassModels.clear();

	for (auto& label : m_Labes)
		delete label;
	m_Labes.clear();

	for (auto& mesh : m_Meshes)
		delete mesh.second;
	m_Meshes.clear();

	for (auto& dirLight : m_DirectionalLights)
		delete dirLight;
	m_DirectionalLights.clear();

	for (auto& pointLight : m_PointLights)
		delete pointLight;
	m_PointLights.clear();

	for (auto& spotLight : m_SpotLights)
		delete spotLight;
	m_SpotLights.clear();

	m_Lights.clear();
}


void RenderingEngine::ClearStaticModels3d()
{
	m_StaticModels.clear();
}

void RenderingEngine::ClearStaticGlassModels3d()
{
	m_StaticGlassModels.clear();
}

void RenderingEngine::ClearStaticLabels()
{
	m_Labes.clear();
}

void RenderingEngine::ClearDirectionalLights()
{
	m_DirectionalLights.clear();
}

void RenderingEngine::ClearPointLights()
{
	m_PointLights.clear();
}

void RenderingEngine::ClearSpotLights()
{
	m_SpotLights.clear();
}
