#ifndef _STATIC_GLASS_MODEL_3D_H
#define _STATIC_GLASS_MODEL_3D_H

#include <string>
#include <glm/glm.hpp>
#include "Mesh.h"
#include "Transform.h"

class StaticGlassModel3D
{
	Transform m_Transform;
	std::string m_Name;
	Mesh* m_pMesh;
	glm::vec4 m_Color;

public:
	StaticGlassModel3D();
	StaticGlassModel3D(const std::string& name, Mesh* mesh, const glm::vec4& color, const Transform& transform);
	~StaticGlassModel3D();

	inline const std::string& getName() { return m_Name; }
	inline Mesh* getMesh() { return m_pMesh; }
	inline glm::vec4& getColor() { return m_Color; }
	inline Transform& getTransform() { return m_Transform; }

};

#endif // _STATIC_GLASS_MODEL_3D_H
