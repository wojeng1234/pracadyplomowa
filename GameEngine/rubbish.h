
/*Shader* shader = re.GetShader(SHADER_TYPE::SPRITE);
tex.bind();
shader->use();
m.Bind();
glDrawArrays(GL_TRIANGLES, 0, m.GetVertexCount());
m.unBind();
shader->unUse();
tex.unBind();*/

//////////////////////////////////////////////////////////////////////////////////////////

/*shader = re.GetShader(SHADER_TYPE::FONT);
fa.getTexture().bind();
shader->use();
glEnable(GL_BLEND);
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
glDisable(GL_DEPTH_TEST);
glBindVertexArray(fa.GetVAO());
std::wostringstream wss;
wss << L"Tekst generowany teraz: ";
wss << i++;
float xPos = 0.0f;
for (wchar_t c : wss.str())
{
	Character& letter = fa.GetCharacter(c);
	glUniform2f(shader->getUniform(UNIFORM::TRANSLATION), xPos, 0.0f);
	glUniform2f(shader->getUniform(UNIFORM::SCR_WIDTH_HEIGHT), 1280, 960);
	glUniform3f(shader->getUniform(UNIFORM::COLOR), 0.0f, 0.0f, 0.0f);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, (fa.getIndicies() + letter.GetPosInBuff() * 6));
	xPos += (letter.GetXadvance() + letter.GetXoffset() + letter.GetWidth()) / 1280;
}
glBindVertexArray(0);
glEnable(GL_DEPTH_TEST);
glDisable(GL_BLEND);
shader->unUse();
fa.getTexture().unBind();*/

////////////////////////////////////////////////////////////////////////////////////

/*glMatrixMode(GL_PROJECTION);
glLoadIdentity();
glOrtho(0, 100, -100, 0, 1, -1);

glMatrixMode(GL_MODELVIEW);
glLoadIdentity();

glBegin(GL_QUADS);
glVertex2f(25.0f, -75.0f);
glVertex2f(25.0f, -25.0f);
glVertex2f(75.0f, -25.0f);
glVertex2f(75.0f, -75.0f);
glEnd();*/

struct Vertex
{
	Vertex(const glm::vec3& p, const glm::vec2& u, const glm::vec3& n)
	: pos(p),
	UV(u),
	normal(n)
	{}
	glm::vec3 pos;
	glm::vec2 UV;
	glm::vec3 normal;
};

const glm::vec3& hsvToRgb(int h, int s, int v)
{
	int f = (h % 60) * 255 / 60;
	int p = (255 - s) * v / 255;
	int q = (255 - f * s / 255) * v / 255;
	int t = (255 - (255 - f) * s / 255) * v / 255;
	int r = 0, g = 0, b = 0;
	switch ((h / 60) % 6)
	{
	case 0: r = v; g = t; b = p; break;
	case 1: r = q; g = v; b = p; break;
	case 2: r = p; g = v; b = t; break;
	case 3: r = p; g = q; b = v; break;
	case 4: r = t; g = p; b = v; break;
	case 5: r = v; g = p; b = q; break;
	}
	return glm::vec3(
		(float)r / 255.0f,
		(float)g / 255.0f,
		(float)b / 255.0f);
}

Vertex verts[] = {
	Vertex(glm::vec3(-0.5f, -0.5f, 0.0f), glm::vec2(0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 0.0f)),
	Vertex(glm::vec3(-0.5f, 0.5f, 0.0f), glm::vec2(0.0f, 1.0f), glm::vec3(1.0f, 0.0f, 0.0f)),
	Vertex(glm::vec3(0.5f, 0.5f, 0.0f), glm::vec2(1.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f)),
	Vertex(glm::vec3(0.5f, -0.5f, 0.0f), glm::vec2(1.0f, 0.0f), glm::vec3(1.0f, 0.0f, 1.0f))
};
glm::vec3 pos[] = {
	glm::vec3(-1.0f, -1.0f, 0.0f),
	glm::vec3(-1.0f, 1.0f, 0.0f),
	glm::vec3(1.0f, 1.0f, 0.0f),
	glm::vec3(1.0f, -1.0f, 0.0f)
};
glm::vec2 coords[] = {
	glm::vec2(0.0f, 0.0f),
	glm::vec2(0.0f, 1.0f),
	glm::vec2(1.0f, 1.0f),
	glm::vec2(1.0f, 0.0f)
};
glm::vec3 normals[] = {
	glm::vec3(1.0f, 1.0f, 0.0f),
	glm::vec3(1.0f, 0.0f, 0.0f),
	glm::vec3(0.0f, 1.0f, 0.0f),
	glm::vec3(1.0f, 0.0f, 1.0f)
};
GLushort indicies[] = {
	0, 1, 2, 0, 2, 3
};
GLuint vboVertices2ID, vboIndicies2ID, vao2ID;
GLuint vboPositionID, vboNormalID, vboCoordID, vboIndiciesID, vaoID;
glGenVertexArrays(1, &vao2ID);
glBindVertexArray(vao2ID);
glGenBuffers(1, &vboVertices2ID);
glGenBuffers(1, &vboIndicies2ID);
glBindBuffer(GL_ARRAY_BUFFER, vboVertices2ID);
glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)* 4, verts, GL_STATIC_DRAW);
glEnableVertexAttribArray(0);
glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
glEnableVertexAttribArray(1);
glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, UV));
glEnableVertexAttribArray(2);
glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, normal));
glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndicies2ID);
glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort)* 6, indicies, GL_STATIC_DRAW);

glGenVertexArrays(1, &vaoID);
glBindVertexArray(vaoID);
glGenBuffers(1, &vboPositionID);
glBindBuffer(GL_ARRAY_BUFFER, vboPositionID);
glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 4, pos, GL_STATIC_DRAW);
glEnableVertexAttribArray(0);
glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), 0);

glGenBuffers(1, &vboCoordID);
glBindBuffer(GL_ARRAY_BUFFER, vboCoordID);
glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * 4, coords, GL_STATIC_DRAW);
glEnableVertexAttribArray(1);
glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), 0);

glGenBuffers(1, &vboNormalID);
glBindBuffer(GL_ARRAY_BUFFER, vboNormalID);
glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 4, normals, GL_STATIC_DRAW);
glEnableVertexAttribArray(2);
glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), 0);

glGenBuffers(1, &vboIndiciesID);
glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndiciesID);
glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLushort)* 6, indicies, GL_STATIC_DRAW);