#include "helpers.h"

bool BothAreSpaces(char lhs, char rhs) 
{ 
	return (lhs == rhs) && (lhs == ' ');
}

std::vector<std::string> split(const std::string& s, char seperator)
{
	std::vector<std::string> output;

	std::string::size_type prev_pos = 0, pos = 0;

	while ((pos = s.find(seperator, pos)) != std::string::npos)
	{
		std::string substring(s.substr(prev_pos, pos - prev_pos));

		output.push_back(substring);

		prev_pos = ++pos;
	}

	output.push_back(s.substr(prev_pos, pos - prev_pos)); // Last word

	return output;
}

std::string UnicodeToAscii(const std::wstring& wstr)
{
	std::string str;
	for (wchar_t c : wstr)
	{
		str += (char)c;
	}

	return str;
}

std::wstring AsciiToUnicode(const std::string& str)
{
	std::wstring wstr;
	for (char c : str)
	{
		wstr += (wchar_t)c;
	}

	return wstr;
}