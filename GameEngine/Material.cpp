#include "Material.h"

#include <Windows.h>
#include <vector>
#include <fstream>
#include "helpers.h"


Material::Material(const glm::vec3& a, const glm::vec3& d, const glm::vec3& s, float shinnes)
:m_Ambient(a, 1.0f),
m_Diffuse(d, 1.0f),
m_Specular(s, shinnes),
m_Shinnes(shinnes)
{}

Material::Material()
{
	m_Ambient = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	m_Diffuse = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	m_Specular = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	m_Shinnes = 1.0f;
	m_Transparency = 0.0f;
}

bool Material::LoadFromFile(const std::string& materialName)
{
	std::ifstream file;
	file.open(std::string("..\\resourecs\\materials\\") + materialName + ".mtl");
	if (!file.good())
	{
		MessageBox(0, L"Material file is not goot", 0, 0);
		return false;
	}

	std::string line;
	while (std::getline(file, line))
	{
		//Podzielenie stringu
		std::vector<std::string> strarr = split(line, ' ');

		if (strarr[0] == "Ns")
		{
			m_Shinnes = atof(strarr[1].c_str());
		}
		else if (strarr[0] == "Ka")
		{
			m_Ambient = glm::vec4(
				atof(strarr[1].c_str()),
				atof(strarr[2].c_str()),
				atof(strarr[3].c_str()),
				1.0f
				);
		}
		else if (strarr[0] == "Kd")
		{
			m_Diffuse = glm::vec4(
				atof(strarr[1].c_str()),
				atof(strarr[2].c_str()),
				atof(strarr[3].c_str()),
				1.0f
				);
		}
		else if (strarr[0] == "Ks")
		{
			m_Specular = glm::vec4(
				atof(strarr[1].c_str()),
				atof(strarr[2].c_str()),
				atof(strarr[3].c_str()),
				1.0f
				);
		}
		else if (strarr[0] == "d")
		{
			m_Transparency = 1.0f - atof(strarr[1].c_str());
		}
	}

	file.close();
	return true;
}

Material::~Material()
{
}