#ifndef _RENDERING_ENGINE_H
#define _RENDERING_ENGINE_H

#include <map>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "MyWindow.h"
#include "Shader.h"
#include "FontAtlas.h"
#include "Texture.h"
#include "Mesh.h"
#include "Material.h"
#include "StaticLabel.h"
#include "StaticModel3D.h"
#include "Transform.h"
#include "Camera.h"
#include "StaticGlassModel3D.h"
#include "Lights.h"

class Engine;

enum REND_INIT
{
	OPEN_GL
};

enum SHADER_TYPE
{
	SPRITE,
	AMBIENT,
	POINT_LIGHT,
	DIRECTION_LIGHT,
	SPOT_LIGHT,
	FONT,
	GLASS,
	LINE_DRAWING
};

enum MATERIAL_TYPE
{
	MAT,
	SHINY
};

typedef std::map<SHADER_TYPE, Shader*> ShadersMap;
typedef std::map<MATERIAL_TYPE, Material*> MaterialsMap;
typedef std::map<std::string, FontAtlas*> FontsMap;
typedef std::map<std::string, Texture*> TexturesMap;
typedef std::map<std::string, Mesh*> MeshesMap;
typedef std::map<std::string, StaticModel3D*> StaticModelsMap;
typedef std::map<std::string, StaticGlassModel3D*> StaticGlassModelsMap;
typedef std::vector<StaticLabel*> StaticLabelsVector;

typedef std::vector<DirectionalLight*> DirectionalLights;
typedef std::vector<PointLight*> PointLights;
typedef std::vector<SpotLight*> SpotLights;
typedef std::vector<Light*> LightsVector;

class RenderingEngine
{
private:
	ShadersMap m_Shaders;
	FontsMap m_Fonts;
	MeshesMap m_Meshes;
	TexturesMap m_Textures;
	MaterialsMap m_Materials;
	StaticModelsMap m_StaticModels;
	StaticGlassModelsMap m_StaticGlassModels;
	StaticLabelsVector m_Labes;

	DirectionalLights m_DirectionalLights;
	PointLights m_PointLights;
	SpotLights m_SpotLights;
	LightsVector m_Lights;

	MyWindow* m_pWindow;
	Engine* m_pEngine;

	Camera* m_pCamera;

	////////////////////////////////////////////////// OpenGL
	HGLRC m_hRC;
	HDC m_hDC;
	HWND m_HWND;
	bool InitGLEW(HINSTANCE hInstance);
	bool InitOpenGL(HINSTANCE hInstance, HWND hWnd);
	bool InitOldOpenGL(HINSTANCE hInstance, HWND hWnd);
	GLuint m_defaultVAO;
	///////////////////////////////////////////////////OpenGL

public:
	RenderingEngine();
	~RenderingEngine();

	Engine* getOwnEngine() { return m_pEngine; }
	Camera* getOwnCamera() { return m_pCamera; }

	bool Init(HINSTANCE hInstance, MyWindow* window, REND_INIT flag, Engine* engine);
	void ClearBuffers(GLbitfield bufflag);
	void SetClearColor(const glm::vec4& color);
	void SwapBuffers();

	//Methods for shaders
	void AddUniformForShader(SHADER_TYPE shaderType, const std::string &uniformName, UNIFORM uniformEnum);
	void AddCamera(Camera* camera);
	//Shader* GetShader(const SHADER_TYPE& shader);

	//Methods for drawing
	void DrawStaticStaff();
	void DrawStaticLabels();
	void DrawStaticModles3D();
	void DrawStaticGlass3D();
	void DrawDynamicLabel(const std::string& fontName, const std::wstring& label, const glm::vec2& translation, const glm::vec3& color, float scale = 1.0f);
	void DrawLine(const glm::vec2& p1, const glm::vec2& p2);

	//Creators
	//StaticModel3D* CreateStaticModel3d(const std::string& modelName, const std::string& meshName, const std::string& materialName, const std::string& textureName);
	//Tymczasowo zwaraca wska�nik.
	void CreateStaticModel3d(const std::string& modelName, const std::string& meshName, const std::string& materialName, const std::string& textureName, Transform transform = Transform());
	void CreateStaticLabel(const std::string& fontName, const std::wstring& label, const glm::vec2& translation, const glm::vec3& color, float scale = 1.0f);
	void CreateFontAtlas(const std::string& fontName);
	void CreateTexture(const std::string& texName);
	void CreateStaticMesh(const std::string& meshName);
	void CreateStaticGlassModel3D(const std::string& modelName, const std::string& meshName, const glm::vec4& color, const Transform& transform);
	void CreateShader(const std::string& shaderName, const SHADER_TYPE& shaderType);
	void CreateMaterial(const std::string& materialName, const MATERIAL_TYPE& materialType);

	void CreateDirectionalLight(
		const glm::vec4 &ambient,
		const glm::vec4 &diffuse,
		const glm::vec4 &specular,
		const glm::vec3 &direction);
	void CreatePointLight(
		const glm::vec4 &ambient,
		const glm::vec4 &diffuse,
		const glm::vec4 &specular,
		const glm::vec3 &pos,
		const float &range,
		const glm::vec3 &att);
	void CreateSpotLight(
		const glm::vec4 &ambient,
		const glm::vec4 &diffuse,
		const glm::vec4 &specular,
		const glm::vec3 &pos,
		const float &range,
		const glm::vec3 &dir,
		const float &spot,
		const glm::vec3 &att);

	void ClearScene();
	void ClearStaticModels3d();
	void ClearStaticGlassModels3d();
	void ClearStaticLabels();
	void ClearDirectionalLights();
	void ClearPointLights();
	void ClearSpotLights();
};

#endif
