#include "Engine.h"
#include "FreeCamera.h"

Engine::Engine()
{
	m_Scene = new Scene();
}


Engine::~Engine()
{
	delete m_pEngineTimer;
	delete m_pRenderingEngine;
	delete m_pWindow;
	delete m_pInput;
	delete m_Scene;
}


void Engine::init(
	MyWindow* window,
	EngineTimer* timer,
	RenderingEngine* rendEngine,
	Input* input,
	HINSTANCE hInstance)
{
	m_pWindow = window;
	m_pEngineTimer = timer;
	m_pRenderingEngine = rendEngine;
	m_pInput = input;
	m_hInstance = hInstance;

	m_pWindow->init(L"Praca dyplomowa", CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, hInstance, this);

	m_pRenderingEngine->Init(hInstance, window, REND_INIT::OPEN_GL, this);
	m_pRenderingEngine->AddCamera(new FreeCamera());
	((FreeCamera*)m_pRenderingEngine->getOwnCamera())->setRotSpeed(0.001f);
	m_pRenderingEngine->SetClearColor(glm::vec4(0.8f, 0.8f, 0.8f, 1.0f)); //Ustalenie koloru czyszczenia tylnego bufora
	/*m_pRenderingEngine->getOwnCamera()->setProjectionMatrix(
		70.0f,
		(float)getWindow()->getWidth() / (float)getWindow()->getHeight(),
		0.1f, 1000.0f);*/

	m_pRenderingEngine->getOwnCamera()->setPerspectiveProjectionMatrix(
		70.0f,
		(float)getWindow()->getWidth() / (float)getWindow()->getHeight(),
		0.1f, 1000.0f);
	m_pRenderingEngine->getOwnCamera()->setOrthogonalProjectionMatrix(
		-(float)getWindow()->getWidth() / 200,
		(float)getWindow()->getWidth() / 200,
		-(float)getWindow()->getHeight() / 200,
		(float)getWindow()->getHeight() / 200,
		-100.0f, 100.0f
		);

	m_pRenderingEngine->getOwnCamera()->getPos().z = -5;
	m_pRenderingEngine->getOwnCamera()->getPos().y = 1;

	m_pRenderingEngine->CreateFontAtlas("ArialPL");
	m_pRenderingEngine->CreateFontAtlas("SylfaenPL");

	m_pEngineTimer->reset();
	m_pEngineTimer->start();
}

void Engine::ProcessRender()
{
	getRenderingEngine()->ClearBuffers(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	getRenderingEngine()->DrawStaticStaff();
	/*m3d2->getTransform().setPosition(glm::vec3(sin(a), cos(a), 0.0f));
	m3d1->getTransform().Rotate(glm::vec3(0.0f, 0.05f, 0.0f));
	a += 0.01f;*/
	getRenderingEngine()->DrawStaticGlass3D();

	getRenderingEngine()->DrawDynamicLabel("SylfaenPL", std::wstring(L"FPS: ") + std::to_wstring((int)(1.0f / getEngineTimer()->deltaTime())),
		glm::vec2(-1.0f, 1.0f), glm::vec3(0.0f, 0.0f, 0.0f), 0.8f);
	getRenderingEngine()->DrawDynamicLabel("SylfaenPL", std::to_wstring(getEngineTimer()->deltaTime()),
		glm::vec2(-1.0f, 0.9f), glm::vec3(0.0f, 0.0f, 0.0f), 0.8f);

	getRenderingEngine()->DrawLine(glm::vec2(0.0, 0.1), glm::vec2(0.0, -0.1));
	getRenderingEngine()->DrawLine(glm::vec2(-0.1, 0.0), glm::vec2(0.1, 0.0));

	getRenderingEngine()->SwapBuffers();
}

void Engine::ProcessInput()
{
	float camSpeed = 5.0f;

	if (getInput()->isKeyDown(VK_X) && getInput()->isMouseLocked())
		getInput()->unlockCursor();
	else if (getInput()->isKeyDown(VK_X) && !getInput()->isMouseLocked())
		getInput()->lockCursor();

	if (getInput()->isMouseLocked())
	{
		if (getInput()->getKeyState(VK_SHIFT))
			camSpeed = 15.0f;
		else
			camSpeed = 5.0f;

		if (getInput()->getKeyState(VK_W))
			((FreeCamera*)getRenderingEngine()->getOwnCamera())->walk(camSpeed * getEngineTimer()->deltaTime());
		if (getInput()->getKeyState(VK_S))
			((FreeCamera*)getRenderingEngine()->getOwnCamera())->walk(camSpeed * -getEngineTimer()->deltaTime());
		if (getInput()->getKeyState(VK_A))
			((FreeCamera*)getRenderingEngine()->getOwnCamera())->strafe(camSpeed * -getEngineTimer()->deltaTime());
		if (getInput()->getKeyState(VK_D))
			((FreeCamera*)getRenderingEngine()->getOwnCamera())->strafe(camSpeed * getEngineTimer()->deltaTime());
		if (getInput()->getKeyState(VK_E))
			((FreeCamera*)getRenderingEngine()->getOwnCamera())->lift(5 * -getEngineTimer()->deltaTime());
		if (getInput()->getKeyState(VK_Q))
			((FreeCamera*)getRenderingEngine()->getOwnCamera())->lift(5 * getEngineTimer()->deltaTime());

		((FreeCamera*)getRenderingEngine()->getOwnCamera())->rotate(
			((FreeCamera*)getRenderingEngine()->getOwnCamera())->getYaw() - getInput()->getXDelta(),
			((FreeCamera*)getRenderingEngine()->getOwnCamera())->getPitch() + getInput()->getYDelta(),
			0.0f);
	}
}